package model;

import model.Excepciones.PaisNoExistenteException;

public class Limitrofe {
	
	Pais paisVerticeA;
	Pais paisVerticeB;
	double distancia;

	public Limitrofe(Pais unPaisInicial, Pais unPaisDestino, int distanciaEntreEllosEnKm) {
		
		this.paisVerticeA = unPaisInicial;
		this.paisVerticeB = unPaisDestino;
		this.distancia = distanciaEntreEllosEnKm;
	}

	public double obtenerDistancia() {
		
		return distancia;
	}

	public Pais obtenerLimitrofeDesde(Pais unPaisInicial) {
		
		if (unPaisInicial.equals(this.paisVerticeA)) return this.paisVerticeB;
		if (unPaisInicial.equals(this.paisVerticeB)) return this.paisVerticeA;
		
		throw new PaisNoExistenteException();
	}
	
	public Pais obtenerDestino() {
		
		return this.paisVerticeB;
	}
}
