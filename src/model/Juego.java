package model;

import java.util.ArrayList;
import java.util.Random;

import model.Excepciones.CaracteristicaNoExistenteException;

public class Juego {

	private static final int RANGO_RANDOM = 8;
	private Tiempo tiempo;
	Mision mision;
	private Pais paisActual;
	Dificultad dificultad;
	private Jugador jugador;
	OrdenDeArresto ordenDeArresto;
	
	public Juego(Jugador unJugador, Dificultad unaDificultad, Mision unaMision, Tiempo unTiempo) {
		
		this.setTiempo(unTiempo);
		this.mision = unaMision;
		this.dificultad = unaDificultad;
		this.setJugador(unJugador);
		this.ordenDeArresto = new OrdenDeArresto(null);
	}

	public void inicializarNivel() {
		

		this.setPaisActual(mision.darPaisDeMision());
		this.mision.darLadron().viajar();
		this.dificultad.agregarLadron(this.mision.darLadron());

		this.ordenDeArresto = new OrdenDeArresto(null);
		this.getJugador().setExpediente(mision.darExpediente());
		
		this.getJugador().setPaisActual(this.getPaisActual());
		
		dificultad.guardarPista(this.getPaisActual(), this.getJugador().obtenerRango());
		getTiempo().resetHoras();	
	}
	
	public void reiniciar() {
		
		try {
			this.mision.nuevaMision();
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
		this.inicializarNivel();
	}

	public void jugadorViajaA(Limitrofe unLimitrofe) {
		this.getJugador().viajarA(unLimitrofe);

		if (this.getJugador().paisActual() == this.mision.darLadron().obtenerPaisActual()) {
			this.getPaisActual().limpiarPistas();
			this.mision.darLadron().viajar();
			dificultad.guardarPista(getJugador().paisActual(), this.getJugador().obtenerRango());
		}
		
		this.setPaisActual(this.getJugador().paisActual());

	}
	
	

	public String obtenerTiempo() {
		
		return this.getTiempo().toString();
	}

	public Ladron getLadron() {
		
		return this.mision.darLadron();
	}

	public Pais paisActual() {
		
		return this.getPaisActual();
	}
	
	public Mision obtenerMision() {
		
		return this.mision;
	}
	
	public Jugador obtenerJugador() {
		
		return this.getJugador();
	}
	
	public boolean terminoElTiempo() {
		
		return this.getTiempo().terminoElTiempo();
	}
	
	public boolean ladronNoPuedeEscaparseMas() {
		
		Ladron ladron = this.mision.darLadron();
		return (ladron.terminoRecorrido() && this.getPaisActual().equals(ladron.obtenerPaisActual()));
	}
	
	public boolean gano() {
		
		Ladron ladron = this.mision.darLadron();
		return (ladronNoPuedeEscaparseMas() && this.getJugador().arrestarConUnaOrden(ordenDeArresto, ladron));	
	} 
	public boolean perdio(){
		
		return this.getTiempo().terminoElTiempo();
	}

	public void generarOrdenDeArresto(
		ArrayList<Caracteristica> caracteristicasDelSospechoso) {
					
		this.ordenDeArresto = this.getJugador().generarOrdenDeArresto(caracteristicasDelSospechoso);
	}

	public boolean terminoElNivel() {

		return (gano() || perdio());
	}
	
	public boolean ordenDeArrestoVacia() {
		
		return this.ordenDeArresto.estaVacia();
	}

	public int horasRestadasPorCuchillazos() {
		
		Random rand = new Random();
		if (rand.nextInt(RANGO_RANDOM) == 0) { 
			return this.getJugador().acuchillar();
		}
		
		return 0;	
	}

	public ObjetoRobado darObjetoRobado() {
		
		return this.mision.darObjetoRobado();
	}

	public Dificultad obtenerDificultad() {
		
		return this.dificultad;
	}

	public void setOrdenDeArresto(OrdenDeArresto ordenDeArresto) {
		
		this.ordenDeArresto = ordenDeArresto;	
	}
	
	public OrdenDeArresto getOrdenDeArresto() {
		
		return this.ordenDeArresto;	
	}


	public Pais getPaisActual() {
		
		return paisActual;
	}

	public void setPaisActual(Pais paisActual) {
		
		this.paisActual = paisActual;
	}

	public Jugador getJugador() {
		
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		
		this.jugador = jugador;
	}

	public Tiempo getTiempo() {
		
		return tiempo;
	}

	public void setTiempo(Tiempo tiempo) {
		
		this.tiempo = tiempo;
	}
	
}
