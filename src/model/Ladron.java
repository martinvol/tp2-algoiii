package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;

import model.Excepciones.CaracteristicaNoExistenteException;
import model.Tipo.*;

public class Ladron {


	private ArrayList<Caracteristica> caracteristicas;
	private RecorridoLadron recorrido;  
	
	public Ladron() {
		
		this.recorrido = new RecorridoLadron();
		this.caracteristicas = new ArrayList<Caracteristica>();
	}

	public boolean viajar() {
		// Devuelve si pudo viajar. Si no tene paises devuelve false.
		return (recorrido.siguientePais());
		
	}
	
	public boolean agregarCaracteristica(Caracteristica caracteristica) {
		
		if (caracteristicas.contains(caracteristica)) return false;
		return caracteristicas.add(caracteristica);
	}

	public boolean agregarRecorrido(Pais pais) {

		return recorrido.agregarPais(pais);
	}
	

	public Pais obtenerPaisActual() {
		
		return recorrido.obtenerPaisActualDelLadron();	
	}
	
	public Caracteristica obtenerCaracteristicaDeTipo(Tipo tipo) throws CaracteristicaNoExistenteException { 
		// Recorre la lista de caracteristica que tiene buscando el tipo que se pidio. 
		// Devuelve Null si no tiene esa caracteristica.
		
		for (Caracteristica caracteristica : this.caracteristicas) {
			if (caracteristica.getTipo().equals(tipo)) return caracteristica;
		}	
		
		throw new CaracteristicaNoExistenteException();
	}
	
	public boolean coincidenciaConCaracteristicas(ArrayList<Caracteristica> caracteristicas) {
				
		if(this.caracteristicas.isEmpty()) return false;
		
		Iterator<Caracteristica> iteradorCaracteristicas;
		
		for (Caracteristica caracteristica : caracteristicas){
			iteradorCaracteristicas = this.caracteristicas.iterator();
		     while (iteradorCaracteristicas.hasNext()) {
		       	 Caracteristica caracteristica2 = iteradorCaracteristicas.next();
		       	 if (caracteristica2.equals(caracteristica)) break;
		       	 if (!iteradorCaracteristicas.hasNext()) return false;			
		     }	
		}
		return true;	
	}
	
	public ArrayList<Caracteristica> obtenerCaracteristicas() {
		
		return this.caracteristicas;
	}
	
	public ArrayList<Caracteristica> obtenerCaracteristicasParaPistas() {
		// Devuelve las caracteristicas del ladron, menos la de tipo nombre
		ArrayList<Caracteristica> caracteristicasParaPistas= new ArrayList<Caracteristica>();
		
		for (Caracteristica caracteristica: this.caracteristicas){
			if (!caracteristica.getTipo().equals(new TipoNombre())) caracteristicasParaPistas.add(caracteristica);
		}
		
		return caracteristicasParaPistas;
	}
	
	public Double paisesRestantes() {
		
		if (this.recorrido.obtenerCantidadDePaises() == 0) return this.recorrido.obtenerCantidadDePaises();
		return ((this.recorrido.obtenerCantidadDePaises()) - 1);
	}
	
	public boolean terminoRecorrido(){
		
		return this.paisesRestantes().equals(0.0);
	}
	
	public boolean equals(Ladron otroLadron) {
		return (this.obtenerCaracteristicas()).equals(otroLadron.obtenerCaracteristicas());
	}
	
	public Queue<Pais> obtenerRecorrido(){
		return this.recorrido.darPaisesRecorrido();
	}
	
}
