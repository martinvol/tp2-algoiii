package model;

public class OrdenDeArresto {
	
	Ladron sospechoso;
	
	public OrdenDeArresto(Ladron unLadron) {
		
		this.sospechoso = unLadron;
	}

	public boolean correspondeA(Ladron unLadron) {
		
		if (this.sospechoso == null) return false;
		return (unLadron.equals(this.sospechoso));
	}
	
	public boolean estaVacia() {
		
		return this.sospechoso == null;
	}
	
	public Ladron obtenerSospechoso() {
		
		return sospechoso;
	}
}
