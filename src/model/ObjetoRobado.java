package model;

import java.util.ArrayList;

import model.Excepciones.CaracteristicaNoExistenteException;
import model.Tipo.Tipo;
import model.Tipo.TipoCantidadPaises;
import model.Tipo.TipoNombre;


public class ObjetoRobado {	
	
	private ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
	
	public boolean agregarCaracteristica(Caracteristica caracteristica) {
		
		if (caracteristicas.contains(caracteristica)) return false;
		return caracteristicas.add(caracteristica);
	}
	
	public Caracteristica obtenerCaracteristica(Tipo tipo) throws CaracteristicaNoExistenteException {
		
		for (Caracteristica caracteristica : this.caracteristicas) {
			if (caracteristica.getTipo().equals(tipo)) return caracteristica;
		}	
		
		throw new CaracteristicaNoExistenteException();
	}

	public Integer obtenerCantidadDePaises() throws NumberFormatException, CaracteristicaNoExistenteException{
		
		return (Integer.parseInt(this.obtenerCaracteristica(new TipoCantidadPaises()).getDescripcion()));
	}
	
	public String obtenerNombre() throws CaracteristicaNoExistenteException{
		
		return this.obtenerCaracteristica(new TipoNombre()).getDescripcion();
	}
	
	public String obtenerDescripcionDelValor() throws CaracteristicaNoExistenteException {
		
		Caracteristica caracteristica = this.obtenerCaracteristica(new TipoCantidadPaises());	
			
		if (caracteristica.getDescripcion().equals("4")) return "Comun";
		if (caracteristica.getDescripcion().equals("5")) return "Valioso";
		return "Muy Valioso";	
	}
}
