package model.Tipo;

public class TipoPistaVacia implements Tipo{
	/*
	 *  Clase que implementa la interfaz Tipo.
	 *  
	 * */

	private String textoParaPista= "Nunca vi a nadie con esa descripcion";
	// String que contiene el texto modelo de la pista
	

	public String darModeloPista(){
		// Metodo devuelve el modelo para formar la pista.
		return this.textoParaPista;
	}

	
	public boolean equals(Tipo tipo){
		// metodo que compara si los dos tipos son del mismo tipo. ( tienen el mismo comportamiento ) 
		return (this.textoParaPista == (tipo.darModeloPista()));
	}
}