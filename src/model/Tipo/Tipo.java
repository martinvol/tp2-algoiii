package model.Tipo;

public interface Tipo {
/* Interfaz de Tipo, simula el tipo de caracteristica, lo que vendria a ser la categoria de la caracteristica.
 *	Y deberia contener como se muestra en forma de pista. 	
 * */
	
	public String darModeloPista();  // Metodo que deberia devolver lo que seria el formato de como mostrarse
	public boolean equals(Tipo tipo);
	
	

}
