package model.Tipo;

public class TipoGente implements Tipo{
	/*
	 *  Clase que implementa la interfaz Tipo.
	 *  
	 * */

	private String textoParaPista= "Parecia muy interesado en la cultura de los ";
	// String que contiene el texto modelo de la pista
	

	public String darModeloPista(){
		// Metodo devuelve el modelo para formar la pista.
		return this.textoParaPista;
	}

	
	public boolean equals(Tipo tipo){
		// metodo que compara si los dos tipos son del mismo tipo. ( tienen el mismo comportamiento ) 
		return (this.textoParaPista == (tipo.darModeloPista()));
	}
}
