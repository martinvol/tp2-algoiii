package model;

public class Tiempo {
	
	private static final int HORAS_SUENIO = 8;
	private static final float HORA_INIT = 7;
	private static final float CANT_HORAS_DIA = 24;
	private float cantidadDeTiempo;
	private float horarioProximoADormir;
	private float cantidadInicialTiempo;
	
	public Tiempo(float cantidad_tiempo){
		this.cantidadDeTiempo = cantidad_tiempo;
		this.cantidadInicialTiempo = cantidad_tiempo;
		// se asume que el tiempo siempre empieza a las
		// 7 horas, por lo que luego de pasar 17hs debemos dormir
		this.horarioProximoADormir = this.cantidadDeTiempo - 17;
	}
	
	public float tiempoRestante() {
		
		return terminoElTiempo()? 0:this.cantidadDeTiempo;
	}

	public void restarHoras(double unaCantidadDeTiempo) {
		
		if (!(this.terminoElTiempo())) this.cantidadDeTiempo -= unaCantidadDeTiempo;
		if (this.cantidadDeTiempo < 0) this.cantidadDeTiempo = 0;
		
		if (this.cantidadDeTiempo <= horarioProximoADormir){
			this.cantidadDeTiempo -= HORAS_SUENIO;
			this.horarioProximoADormir -= CANT_HORAS_DIA;
		}
	}

	public boolean terminoElTiempo() {
		
		return (this.cantidadDeTiempo <= 0);
	}

	public void resetHoras() {

		this.cantidadDeTiempo = this.cantidadInicialTiempo;		
	}
	
	public String toString(){
		int horaActual = (int) (((this.cantidadInicialTiempo - this.cantidadDeTiempo + HORA_INIT) % CANT_HORAS_DIA));
		int indicadorDiaActual = (int) ((this.cantidadInicialTiempo - this.cantidadDeTiempo + HORA_INIT) / (CANT_HORAS_DIA));
		String dia;
		switch (Math.abs(indicadorDiaActual)) {
		case 0:
			dia = "Lunes, ";
			break;
		case 1:
			dia = "Martes, ";
			break;
		case 2:
			dia = "Miercoles, ";
			break;
		case 3:
			dia = "Jueves, ";
			break;
		case 4:
			dia = "Viernes, ";
			break;
		case 5:
			dia = "Sabado, ";
			break;
		case 6:
			dia = "Domingo, ";
			break;
		default:
			dia = "";
			break;			
		}
			
		return dia + (horaActual) + " hs";
	}

}
