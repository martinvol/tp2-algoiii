package model;

import java.util.ArrayList;
import java.util.Random;

import model.Excepciones.CaracteristicaNoExistenteException;
import model.Excepciones.ContenedorVacioException;
import model.Tipo.TipoNombre;

public class BancoObjetosRobados {
	
	ArrayList<ObjetoRobado> ObjetosRobados = new ArrayList<ObjetoRobado>();
	
	public ObjetoRobado darUnObjetoRobadoRandom() {
		
		if (ObjetosRobados.isEmpty()) throw new ContenedorVacioException();
		Random rnd = new Random();
		
		return ObjetosRobados.get((int) (rnd.nextDouble()*ObjetosRobados.size()));
	}
	
	public Boolean agregarObjetoRobado(ObjetoRobado objetoRobado) {
		
		if (this.ObjetosRobados.contains(objetoRobado)) return false;
		return ObjetosRobados.add(objetoRobado);
		}
	
	public ObjetoRobado darUnObjetoRobadoConNombre(Caracteristica caracteristica) throws CaracteristicaNoExistenteException {
		
		for(ObjetoRobado objeto : this.ObjetosRobados){
			if(objeto.obtenerCaracteristica(new TipoNombre()).equals(caracteristica)) return objeto;
		}

		throw new CaracteristicaNoExistenteException();
	}
	
}
