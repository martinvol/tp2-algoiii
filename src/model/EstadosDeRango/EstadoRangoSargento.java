package model.EstadosDeRango;

import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoCuatro;

public class EstadoRangoSargento implements EstadoRango {

	private static final int VELOCIDAD_EN_KMH = 1500;

	@Override
	public DificultadGrado darGradoDeDificultad() {
		return new DificultadGradoCuatro();
	}

	@Override
	public double obtenerTiempoDeViaje(double d) {
		
		return d/VELOCIDAD_EN_KMH;
	}

	@Override
	public String obtenerCargo() {
		
		return "Sargento";
	}
}
