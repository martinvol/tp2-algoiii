package model.EstadosDeRango;

import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoTres;

public class EstadoRangoInvestigador implements EstadoRango {

	private static final int VELOCIDAD_EN_KMH = 1300;

	@Override
	public DificultadGrado darGradoDeDificultad() {
		return new DificultadGradoTres();
	}

	@Override
	public double obtenerTiempoDeViaje(double d) {
		
		return d/VELOCIDAD_EN_KMH;
	}

	@Override
	public String obtenerCargo() {
		
		return "Investigador";
	}
}
