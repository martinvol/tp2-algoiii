package model.EstadosDeRango;

import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoUno;

public class EstadoRangoNovato implements EstadoRango {

	private static final int VELOCIDAD_EN_KMH = 900;

	@Override
	public DificultadGrado darGradoDeDificultad() {
		return new DificultadGradoUno();
	}

	@Override
	public double obtenerTiempoDeViaje(double d) {
		
		return d/VELOCIDAD_EN_KMH;
	}

	@Override
	public String obtenerCargo() {
		
		return "Novato";
	}

}
