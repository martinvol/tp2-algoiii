package model.EstadosDeRango;

import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoDos;

public class EstadoRangoDetective implements EstadoRango {

	private static final int VELOCIDAD_EN_KMH = 1100;

	@Override
	public DificultadGrado darGradoDeDificultad() {
		return new DificultadGradoDos();
	}

	@Override
	public double obtenerTiempoDeViaje(double d) {
		
		return d/VELOCIDAD_EN_KMH;
	}

	@Override
	public String obtenerCargo() {
		
		return "Detective";
	}
}
