package model.EstadosDeRango;

import model.GradoDificultad.DificultadGrado;

public interface EstadoRango {
	public DificultadGrado darGradoDeDificultad();
	public double obtenerTiempoDeViaje(double d);
	public String obtenerCargo();
}
