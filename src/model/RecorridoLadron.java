package model;

import java.util.LinkedList;
import java.util.Queue;

public class RecorridoLadron {

	private Queue<Pais> paises;
	private Double cantidad;
	
	public RecorridoLadron() {
		
		this.paises=new LinkedList<Pais>();
		this.cantidad=0.0;
	}
	
	public Queue<Pais> darPaisesRecorrido() {
		
		return this.paises;
	}
	
	public boolean siguientePais() {
		// Devuelve si pudo viajar. Si no tene paises devuelve false.
		if (cantidad > 1) {
			paises.poll(); 
			cantidad --;
			return true;
		}
		
		return false;	
	}

	public boolean agregarPais(Pais pais) {
		// Agrega el recorrido que tiene que hacer el Ladron.

		if(paises.offer(pais)) {
			cantidad ++;
			return true;
		}
		
		return false;
	}
	

	public Pais obtenerPaisActualDelLadron() {
		
		return paises.peek();
	}
	
	public Double obtenerCantidadDePaises(){
		
		return this.cantidad;
	}
}


