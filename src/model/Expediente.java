package model;

import java.util.ArrayList;
import java.util.Random;

import model.Excepciones.ContenedorVacioException;

public class Expediente {
	
	ArrayList<Ladron> expediente = new ArrayList<Ladron>();
	
	public boolean agregarLadron(Ladron ladron) {
		
		if(this.expediente.contains(ladron)) return false;
		
		return this.expediente.add(ladron);
	}

	public boolean estaVacio() {
		
		return this.expediente.isEmpty();
	}

	public ArrayList<Ladron> verCoincidencias(ArrayList<Caracteristica> caracteristicas) {
		// Metodo que en base a unas caracteristicas devuelve los ladrones que contienen estas caracteristicas.
		
		ArrayList<Ladron> ladronesConCoincidencias = new ArrayList<Ladron>();
		if(this.expediente.isEmpty()) return ladronesConCoincidencias;
		
		for (Ladron ladron : this.expediente) {
			if(ladron.coincidenciaConCaracteristicas(caracteristicas)) ladronesConCoincidencias.add(ladron); 
		}
		
		return ladronesConCoincidencias;	
	}
	
	public Ladron darLadronRandom(){
		// Metodo que devuelve en forma Random un ladron de los que contiene expediente.
		if (this.expediente.isEmpty()) throw new ContenedorVacioException();
		Random rnd = new Random();
		
		return this.expediente.get(rnd.nextInt(this.expediente.size()));
	}
}
