package model;

import model.GradoDificultad.*;
import model.Tipo.*;

public class Caracteristica {
	
	Tipo tipo;
	String descripcion;
	DificultadGrado dificultad;

	public Caracteristica(Tipo tipo, String descripcion, DificultadGrado dificultad) {
		
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.dificultad = dificultad;
	}

	public String getDescripcion() {
		
		return this.descripcion;
	}

	public Tipo getTipo() {
		
		return this.tipo;
	}

	public DificultadGrado getDificultad() {
		
		return this.dificultad;
	}

	public boolean equals(Caracteristica otraCaracteristica) {
		// Compara dos caracteristicas, por el tipo, por la descripcion y por la dificultad.
		return ((this.tipo.equals(otraCaracteristica.getTipo())) && 
				(this.dificultad.equals(otraCaracteristica.getDificultad())) &&
				(this.descripcion.equals(otraCaracteristica.getDescripcion())));
	}
	
	public String mostrarComoPista(){
		
		return (tipo.darModeloPista() + this.descripcion);
	}
	
	public String toString(){
		
		return this.mostrarComoPista();
	}
}
