package model;

import java.util.ArrayList;

public class Jugador {
	
	private static final double TIEMPO_GENERAR_ORDEN_ARRESTO = 3;
	Tiempo tiempo;
	Rango rango;
	Pais paisActual;
	Expediente expediente;
	int cantVecesQuePregunta;
	int cantidadChuchillazos = 0;
	
	public Jugador(Tiempo tiempo) {
		
		this.rango = new Rango();
		this.paisActual = null;
		this.tiempo= tiempo;
	}
	
	
	public int tiempoRestante(){
		
		return (int) (tiempo.tiempoRestante());
	}
	
	public ArrayList<Limitrofe> consultarPaisesParaViajar() {
		
		return this.paisActual.getLimitrofes();
	}
	
	public Pais paisActual() {
		
		return this.paisActual;
	}
	
	public void setPaisActual(Pais pais){
		
		this.paisActual = pais;
		this.cantVecesQuePregunta = 0;
	}
		
	public void aumentarArresto(){
	
		this.rango.agregarArresto();	
	}


	public OrdenDeArresto generarOrdenDeArresto(ArrayList<Caracteristica> caracteristicasDeUnLadron) {
				
		this.tiempo.restarHoras(TIEMPO_GENERAR_ORDEN_ARRESTO);
		ArrayList<Ladron> posiblesLadrones = this.expediente.verCoincidencias(caracteristicasDeUnLadron);
				
		if (posiblesLadrones.size() == 1) {
			return (new OrdenDeArresto(posiblesLadrones.get(0)));
		}
		
		return (new OrdenDeArresto(null));
	}

	public int cantidadDeArrestos() {
		 
		return this.rango.getCantidadDeArrestos();
	}


	public void viajarA(Limitrofe unLimitrofe) {
		
		this.tiempo.restarHoras((int) this.rango.obtenerTiempoDeViaje(unLimitrofe.obtenerDistancia()));
		this.paisActual = unLimitrofe.obtenerLimitrofeDesde(this.paisActual);
		this.cantVecesQuePregunta = 0;
	}


	public Rango obtenerRango() {

		return this.rango;
	}


	public boolean arrestarConUnaOrden(OrdenDeArresto orden, Ladron sospechoso) {
		
		if (orden.correspondeA(sospechoso)) {
			this.rango.agregarArresto();
		}
		
		return orden.correspondeA(sospechoso);
	}


	public void setExpediente(Expediente unExpediente) {

		this.expediente = unExpediente;
	}


	public ArrayList<Caracteristica> obtenerPistasBiblioteca() {
		restarHorasAlPreguntar();
		
		return this.paisActual.obtenerBiblioteca().darPistas();
	}


	public ArrayList<Caracteristica> obtenerPistasAeropuerto() {
		restarHorasAlPreguntar();
		
		return this.paisActual.obtenerAeropuerto().darPistas();
	}


	public ArrayList<Caracteristica> obtenerPistasBanco() {
		restarHorasAlPreguntar();
		
		return this.paisActual.obtenerBanco().darPistas();
	}


	private void restarHorasAlPreguntar() {
		
		if (this.cantVecesQuePregunta < 3) this.cantVecesQuePregunta++;
		this.tiempo.restarHoras(this.cantVecesQuePregunta);
	}


	public int acuchillar() {
		
		if (this.cantidadChuchillazos > 2){
			this.tiempo.restarHoras(3);
			return 3;
		} 
			this.cantidadChuchillazos++;
			this.tiempo.restarHoras(this.cantidadChuchillazos);
			return this.cantidadChuchillazos;
	}
}
