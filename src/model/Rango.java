package model;

import model.EstadosDeRango.*;
import model.GradoDificultad.*;

public class Rango {

	private static final int CANT_INVESTIGADOR_ARRESTOS = 10;
	private static final int CANT_DETECTIVE_ARRESTOS = 5;
	private static final int CANT_SARGENTO_ARRESTOS = 20;

	private Integer cantidadArrestos;
	private EstadoRango estadoRango = new EstadoRangoNovato();
	
	public Rango() {
		cantidadArrestos = 0;
	}

	public Integer getCantidadDeArrestos() {
		
		return cantidadArrestos;
	}
	
	public void agregarArresto() {
		
		cantidadArrestos ++;
		this.estadoRango = setEstadoCorrespondiente();
	}
	
	
	public DificultadGrado darGradoDeDificultad() {
		
		return this.estadoRango.darGradoDeDificultad();
	}


	public double obtenerTiempoDeViaje(double d) {
		
		return this.estadoRango.obtenerTiempoDeViaje(d);	
	}
	
	public boolean setArrestos(int arrestos) {
		
		cantidadArrestos = arrestos;
		this.estadoRango = setEstadoCorrespondiente();
		return true;
	}
	
	private EstadoRango setEstadoCorrespondiente() {
		if(cantidadArrestos < CANT_DETECTIVE_ARRESTOS) return new EstadoRangoNovato();
		if(cantidadArrestos < CANT_INVESTIGADOR_ARRESTOS) return new EstadoRangoDetective();
		if(cantidadArrestos < CANT_SARGENTO_ARRESTOS) return new EstadoRangoInvestigador();
		return this.estadoRango = new EstadoRangoSargento();
		
	}

	public String obtenerCargo() {
		
		return this.estadoRango.obtenerCargo();
	}
}
