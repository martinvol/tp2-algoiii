package model;
import java.util.*;

import model.Excepciones.PaisNoExistenteException;

public class Mapa {
	
	// Clase encargada de contener todos los paises del juego.
	
	ArrayList<Pais> mapa = new ArrayList<Pais>();
	
	public boolean agregarPais(Pais pais) {
		
		if (this.mapa.contains(pais)) return false;
		return this.mapa.add(pais);
	}
	
	public ArrayList<Pais> getMapa() {
		
		return mapa;
	}

	public Pais obtenerPaisPorCiudad(Caracteristica caracteristica) {
		// Metodo que le pasan una caracteristica y tiene que devolver el paise que tiene esa caracteristica.
		// Se usa para conseguir el Pais que tenga determinada Ciudad.
		for (Pais pais: this.mapa) {
			if (pais.contieneCaracteristica(caracteristica)) return pais;
		}
		
		throw new PaisNoExistenteException();
	}
	
	public boolean limpiarPaises() {
		// Metodo que limpia todas las pistas de todos los paises del mapa.
		for(Pais pais: this.mapa) {
			if(!pais.limpiarPistas()) return false;
		}
		return true;
	}

}
