package model;

import java.util.ArrayList;
import java.util.Random;

import model.Excepciones.CaracteristicaNoExistenteException;
import model.Tipo.TipoCiudad;

public class Mision {
	// Clase encargada de generar una mision, elegir un objeto, un ladron y un pais, para que el juego inicie.
	
	Ladron ladron = null;
	ObjetoRobado objetoRobado = null;
	Pais paisDeMision = null;
	ArrayList<Pais> recorridoLadron = null; //recorrido completo del ladron
	
	Mapa mapa = null;
	private BancoObjetosRobados bancoObjetosRobados = null;
	Expediente expedientesLadrones = null;
	
	public void agregarMapa(Mapa mapa) {
		
		this.mapa=mapa;	
	}
	
	public void agregarExpedientes(Expediente expediente) {
		
		this.expedientesLadrones = expediente;
	}
	
	public void agregarBancoObjetosRobados(BancoObjetosRobados objetosRobados) {
		
		this.setBancoObjetosRobados(objetosRobados);
	}
	
	public Ladron darLadron() {
		
		return this.ladron;
	}

	
	public ObjetoRobado darObjetoRobado() {
		
		return this.objetoRobado;
	}
	
	public Pais darPaisDeMision() {
		
		return this.paisDeMision;
	}
	
	public boolean cargarMision(Ladron ladron, ObjetoRobado objeto, 
			BancoObjetosRobados bancoObjetosRobados, 
			Expediente expediente, Mapa mapa, ArrayList<Pais> recorridoCompleto, 
			ArrayList<Pais> recorrido) throws CaracteristicaNoExistenteException {
		
		this.agregarMapa(mapa);
		this.agregarBancoObjetosRobados(bancoObjetosRobados);
		this.agregarExpedientes(expediente);
		
		this.objetoRobado = objeto;
		this.paisDeMision= this.mapa.obtenerPaisPorCiudad(this.objetoRobado.obtenerCaracteristica(new TipoCiudad()));
		this.ladron = ladron;
		this.recorridoLadron=recorridoCompleto;
		
		for(Pais pais: recorrido) {
			this.ladron.agregarRecorrido(pais);
		}
		
		return true;
	}
	
	public boolean nuevaMision() throws CaracteristicaNoExistenteException {
		// Metodo que crea una nueva mision, elige un nuevo objeto robado, un nuevo ladron y un nuevo pais.
		// Dandole un recorrido predefinido al ladron.
		
		if(this.mapa == null || this.expedientesLadrones == null || this.getBancoObjetosRobados() == null) return false;
		
		this.mapa.limpiarPaises();
		this.objetoRobado= this.getBancoObjetosRobados().darUnObjetoRobadoRandom();
		this.paisDeMision= this.mapa.obtenerPaisPorCiudad(this.objetoRobado.obtenerCaracteristica(new TipoCiudad()));
		this.ladron = this.expedientesLadrones.darLadronRandom();
		
		
		return this.crearRecorridoLadron(this.ladron,paisDeMision,this.objetoRobado.obtenerCantidadDePaises());
	}
	
	
	private boolean crearRecorridoLadron(Ladron ladron, Pais paisInicial,Integer cantidadPaises) {
		// Metodo que crea el recorrido del ladron basandose en el pais del objeto robado como inicial.
		ArrayList<Pais> recorrido = new ArrayList<Pais>();
		
		Random rnd = new Random();
		Limitrofe limitrofe;	
		ArrayList<Limitrofe> limitrofes;
		Pais paisLimitrofe=paisInicial;
		Pais paisLimitrofe2;
		
		recorrido.add(paisLimitrofe);

		for(int i=0; i < cantidadPaises-1; i++){
		
			limitrofes=paisLimitrofe.getLimitrofes();
			
			if(limitrofes.isEmpty()) return false;
			
			limitrofe = limitrofes.get((int) (rnd.nextDouble()*(limitrofes.size())));
			paisLimitrofe2 = limitrofe.obtenerLimitrofeDesde(paisLimitrofe);
			if(!recorrido.contains(paisLimitrofe2)){
				recorrido.add(paisLimitrofe2);
				paisLimitrofe=paisLimitrofe2;
			}
			else i--;
				
			}
		
		this.recorridoLadron = recorrido;
			
		for(Pais pais: recorrido){
			ladron.agregarRecorrido(pais);
		}
		
		return true;	
	}
	
	public Expediente darExpediente() {
		
		return this.expedientesLadrones;
	}
	
	public BancoObjetosRobados getBancoObjetosRobados() {
		
		return bancoObjetosRobados;
	}
	
	public void setBancoObjetosRobados(BancoObjetosRobados bancoObjetosRobados) {
		
		this.bancoObjetosRobados = bancoObjetosRobados;
	}	
	
	public ArrayList<Pais> obtenerRecorridoLadronCompleto() {
		
		return this.recorridoLadron;
	}
}
