package model;

import java.util.*;

import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.Lugares.*;
import model.Tipo.TipoCiudad;

public class Pais {
	
	private ArrayList<Limitrofe> limitrofes = new ArrayList<Limitrofe>();
	private ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
	private ArrayList<Lugar> lugares= new ArrayList<Lugar>(); 

	public Pais() {
		this.agregarLugares();
	}
	

	public void agregarLugares(){
		
		lugares.add(new LugarBanco());
		lugares.add(new LugarBiblioteca());
		lugares.add(new LugarAeropuerto());
	}
	
	public Boolean agregarCaracteristica(Caracteristica caracteristica){
		
		if (caracteristicas.contains(caracteristica)) return false;
		return this.caracteristicas.add(caracteristica);
	}
	
	public ArrayList<Caracteristica> obtenerCaracteristicas() {
		
		return this.caracteristicas;
	}
	
	
	public boolean limpiarPistas(){
		// Metodo que limpia las pistas de los lugares que tiene el pais.
		for(Lugar lugar: this.lugares){
			lugar.limpiarPistas();
		}
		return true;
	}
	
	public ArrayList<Lugar> obtenerLugares(){
		
		return this.lugares;
	}
	
	public ArrayList<Caracteristica> obtenerCaracteristicasDificultad(DificultadGrado dificultad) {
		// Metodo que devuelve las caracteristicas que contiene el pais con el nivel de dificultad pasada por parametro 
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		if(this.caracteristicas.isEmpty()) return caracteristicas;
		
		for (Caracteristica caracteristica: this.caracteristicas){
			if(caracteristica.getDificultad().equals(dificultad)) caracteristicas.add(caracteristica); 
		}	
		return caracteristicas;
		
	}

	public boolean contieneCaracteristica(Caracteristica caracteristica) {
		
		for(Caracteristica caract: this.caracteristicas){
			if(caract.equals(caracteristica)) return true;
		}
		return false;
	}
		
	public boolean setLimitrofe(Limitrofe pais) {
		
		if (this.limitrofes.contains(pais)) return false;
		return this.limitrofes.add(pais);
	}

	public ArrayList<Limitrofe> getLimitrofes(){
		
		return this.limitrofes;	
	}
	
	public Lugar obtenerBanco() {
		
		return this.lugares.get(0);
	}
	
	public Lugar obtenerBiblioteca() {
		
		return this.lugares.get(1);
	}
	
	public Lugar obtenerAeropuerto() {
		
		return this.lugares.get(2);
	}
	
	public String toString(){
		
		for(Caracteristica caracteristica: this.caracteristicas) {
			if (caracteristica.getTipo().equals(new TipoCiudad())) {
				return caracteristica.getDescripcion();
			}
		}

		return "";
	}
}