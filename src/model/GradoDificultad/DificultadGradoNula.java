package model.GradoDificultad;

public class DificultadGradoNula extends DificultadGrado{

	
	private String comparador= "0";
	
	
	public String darComparador(){
		// Devuelve el comparardor.
		return this.comparador;
	}
	
	public boolean equals(DificultadGrado grado){
		// Compara dos instancias de DificultadGrado
		return (this.comparador == (grado.darComparador()));
	}
	
	public DificultadGrado darUnGradoMenos(){
		return null;
	}
}
