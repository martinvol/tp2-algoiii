package model.GradoDificultad;



public class DificultadGradoDos extends DificultadGrado{
	
	private String comparador= "2";
	
	public String darComparador(){
		// Devuelve el comparardor.
		return this.comparador;
	}
	
	public boolean equals(DificultadGrado grado){
		// Compara dos instancias de DificultadGrado
		return (this.comparador == (grado.darComparador()));
	}
	
	public DificultadGrado darUnGradoMenos(){
		return new DificultadGradoUno();
	}
}
