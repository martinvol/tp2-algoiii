package model.GradoDificultad;

public class DificultadGradoCuatro extends DificultadGrado{
	
	private String comparador= "4";
	
	public String darComparador(){
		// Devuelve el comparardor.
		return this.comparador;
	}
	
	public boolean equals(DificultadGrado grado){
		// Compara dos instancias de DificultadGrado
		return (this.comparador == (grado.darComparador()));
	}
	public DificultadGrado darUnGradoMenos(){
		return new DificultadGradoTres();
	}

}
