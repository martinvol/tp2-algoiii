package model.GradoDificultad;



public class DificultadGradoTres extends DificultadGrado{
	
	private String comparador= "3";
	
	
	public String darComparador(){
		// Devuelve el comparardor.
		return this.comparador;
	}
	
	public boolean equals(DificultadGrado grado){
		// Compara dos instancias de DificultadGrado
		return (this.comparador == (grado.darComparador()));
	}
	
	public DificultadGrado darUnGradoMenos(){
		return new DificultadGradoDos();
	}

}
