package model.GradoDificultad;


public abstract class DificultadGrado {

	
	 private static final String DIFICULTAD_LADRON = "0";
	 private static final String DIFICULTAD_UNO = "1";
	 private static final String DIFICULTAD_DOS = "2";
	 private static final String DIFICULTAD_TRES = "3";
	 private static final String DIFICULTAD_CUATRO = "4";
	 
	 private static DificultadGrado gradoDificultadNula =new DificultadGradoNula();
	 private static DificultadGrado gradoDificultadUno=new DificultadGradoUno();
	 private static DificultadGrado gradoDificultadDos=new DificultadGradoDos();
	 private static DificultadGrado gradoDificultadTres= new DificultadGradoTres();
	 private static DificultadGrado gradoDificultadCuatro= new DificultadGradoCuatro();
	 
	 
	 public static DificultadGrado darDificultad(String tipo) {
		 //  Metodo de clase que devuelve una instancia de una clase de tipo GradoDificultad.
			if(tipo.equals(DIFICULTAD_LADRON)) return gradoDificultadNula;
			if(tipo.equals(DIFICULTAD_UNO)) return gradoDificultadUno;
			if(tipo.equals(DIFICULTAD_DOS)) return gradoDificultadDos;
			if(tipo.equals(DIFICULTAD_TRES)) return gradoDificultadTres;
			if(tipo.equals(DIFICULTAD_CUATRO)) return gradoDificultadCuatro;
			return null;
	 }

		
	abstract public String darComparador();
		
	abstract public boolean equals(DificultadGrado grado);
	
	abstract public DificultadGrado darUnGradoMenos();

	 
}
