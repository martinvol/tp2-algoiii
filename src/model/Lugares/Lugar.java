package model.Lugares;

import java.util.ArrayList;

import model.Caracteristica;

public interface Lugar {

	
	public ArrayList<Caracteristica> darPistas();  // Metodo que devuelve una Lista con las pistas se que dejo en un Lugar o una pista del tipo Vacia.
	public Boolean agregarCaracteristica(Caracteristica caracteristica); // Agrega caracteristica ( Se guarda una Pista ).
	public Boolean limpiarPistas();  // Metodo que borra las pistas guardadas.
	public void agregarPistaVacia();  // Metodo que guarda una caracteristia del tipo PistaVacia..que es la pista por default si nadie le carga pistas.
	public boolean sinPistas();  // Metodo que devuelve si no se le asigno ninguna pista.
	
}
