package model.Lugares;

import java.util.ArrayList;

import model.Caracteristica;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;


public class LugarAeropuerto implements Lugar{

	ArrayList<Caracteristica> Pistas= new ArrayList<Caracteristica>();
	ArrayList<Caracteristica> PistaVacia= new ArrayList<Caracteristica>();
	ArrayList<Tipo> tiposAceptados = new ArrayList<Tipo>();
	
	
	public LugarAeropuerto(){
		this.agregarTiposAceptados();
		this.agregarPistaVacia();
	}
	public ArrayList<Caracteristica> darPistas(){
		if(this.Pistas.isEmpty()) return this.PistaVacia;
		return Pistas;
	}
	public Boolean agregarCaracteristica(Caracteristica caracteristica){
		
		for(Tipo tipo: this.tiposAceptados){
			if(tipo.equals(caracteristica.getTipo()) && !Pistas.contains(caracteristica)) return Pistas.add(caracteristica); 
		}
		return false;
	}
	
	public void agregarPistaVacia(){
		
		this.PistaVacia.add(new Caracteristica(new TipoPistaVacia(),"",DificultadGrado.darDificultad("0")));
	}
	
	public Boolean limpiarPistas(){
		this.Pistas.removeAll(this.Pistas);
		return true;

	}
	
	public void agregarTiposAceptados(){
		tiposAceptados.add(new TipoBandera());
		tiposAceptados.add(new TipoPuntosDeInteres());
		tiposAceptados.add(new TipoGente());
		tiposAceptados.add(new TipoSexo());
		tiposAceptados.add(new TipoPasaTiempo());
		tiposAceptados.add(new TipoPelo());
		tiposAceptados.add(new TipoCaracteristica());
		tiposAceptados.add(new TipoVehiculo());
		tiposAceptados.add(new TipoMisc());
	}
	
	public boolean sinPistas(){
		return Pistas.isEmpty();
	}

}
