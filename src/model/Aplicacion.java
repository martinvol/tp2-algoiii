package model;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import model.Excepciones.CaracteristicaNoExistenteException;

import org.xml.sax.SAXException;

import parser.Parser;
import view.Vista;
import controller.Controller;

public class Aplicacion {
	
    private static final int TIEMPO_TOTAL = 154;

	public static void main(String args[]) throws ParserConfigurationException, TransformerException, SAXException, IOException {

    	Juego juego = empezarJuego();   	
    	Controller controlador = new Controller();
    	controlador.agregarModelo(juego);
    	
    	Vista vista = new Vista(controlador, juego);
    }

    private static Juego empezarJuego() throws ParserConfigurationException, TransformerException, SAXException, IOException {
    	
    	Tiempo tiempo = new Tiempo(TIEMPO_TOTAL);
    	Jugador jugador = new Jugador(tiempo);
    	    	
    	Mision mision = new Mision(); 
    	Parser parser = new Parser();
    	
    	Mapa mapa = parser.crearMapa("resources/datos/paises.xml");
    	parser.agregarLimitrofes(mapa, "resources/datos/limitrofes.xml");
    	mision.agregarBancoObjetosRobados(parser.crearBancoObjetosRobados("resources/datos/objetos.xml"));
    	mision.agregarExpedientes(parser.crearExpediente("resources/datos/ladrones.xml"));
    	mision.agregarMapa(mapa);
    	try {
			mision.nuevaMision();
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
    	
    	Dificultad dificultad = new Dificultad();
    	dificultad.agregarLadron(mision.darLadron());
    	
    	Juego juego = new Juego(jugador, dificultad , mision, tiempo);
    	
    	juego.inicializarNivel();
    	return juego;
    }
}
