package model;

import java.util.ArrayList;
import java.util.Random;

import model.GradoDificultad.DificultadGrado;
import model.Lugares.Lugar;
import model.Tipo.TipoPistaVacia;

public class Dificultad {
	//Clase encargada de guardar las pistas en los paises para que el jugador pueda atrapar al Ladron.
	 
	Ladron ladron = null;
	ArrayList<Caracteristica> caracteristicasUsadas = new ArrayList<Caracteristica>();
	
	public boolean agregarLadron(Ladron ladron) {
		
		this.ladron = ladron;
		this.caracteristicasUsadas.removeAll(caracteristicasUsadas);
		return true;
	}

	
	public void setearCaracteristicasUsadas(ArrayList<Caracteristica> caracteristicasDificultad){
		this.caracteristicasUsadas = caracteristicasDificultad;
	}

	public boolean guardarPista(Pais pais, Rango rango) {
		// Metodo que guarda pistas en el pais pasado por parametro, 
		// y del Grado de dificultad correspondiente al rango pasado por parametro.
		Pais paisLadron= this.ladron.obtenerPaisActual();
		ArrayList<Caracteristica> caracteristicasPaisDelLadron = 
				paisLadron.obtenerCaracteristicasDificultad(rango.darGradoDeDificultad());
		
		if (caracteristicasPaisDelLadron.isEmpty())
			caracteristicasPaisDelLadron = 
					paisLadron.obtenerCaracteristicasDificultad((rango.darGradoDeDificultad()).darUnGradoMenos());
		
		if (pais.equals(this.ladron.obtenerPaisActual()))
			caracteristicasPaisDelLadron= new ArrayList<Caracteristica>();
		
		for (Lugar lugar: pais.obtenerLugares()) {
			for (Caracteristica caracteristica: caracteristicasPaisDelLadron) {
				lugar.agregarCaracteristica(caracteristica);
			}
			
			if(lugar.sinPistas()) lugar.agregarCaracteristica(this.getPistaLadron());	
		}
		
		ArrayList<Lugar> lugares = pais.obtenerLugares();
		Random rnd = new Random();
		Lugar lugar = lugares.get(rnd.nextInt(lugares.size()));
		lugar.agregarCaracteristica(this.getPistaLadron());
		
		return true;
	}
	
	public Caracteristica getPistaLadron(){
		// Metodo que devuelve una pista del ladron, acorde a la cantidad de paises 
		// que le quedan al ladron y a las caracteristicas que no se dieron aun.
		Random rnd = new Random();
		Double cantidadPaises = this.ladron.paisesRestantes();
		ArrayList<Caracteristica> caracteristicasLadron = ladron.obtenerCaracteristicasParaPistas();
		
		if(this.caracteristicasUsadas.isEmpty() && !(caracteristicasLadron.isEmpty())){

			Caracteristica caracteristica = caracteristicasLadron.get((int) (rnd.nextDouble()*caracteristicasLadron.size()));
			this.caracteristicasUsadas.add(caracteristica);
			return caracteristica;
		}
		
		Double restoDePistas =((double) (caracteristicasLadron.size()) - ((double) this.caracteristicasUsadas.size()));
		
		
		if(restoDePistas/2 > cantidadPaises/2){
			
			Caracteristica caracteristica; 
			
			do {
				caracteristica = caracteristicasLadron.get((int) (rnd.nextDouble()*(caracteristicasLadron.size())));
			} while(caracteristicasUsadas.contains(caracteristica)); 
			
			this.caracteristicasUsadas.add(caracteristica);
			return caracteristica;	
		}
		
		Caracteristica caracteristica = new Caracteristica(new TipoPistaVacia(),"",DificultadGrado.darDificultad("0"));
		if (!(caracteristicasUsadas.isEmpty())) 
			caracteristica = caracteristicasUsadas.get((int) (rnd.nextDouble()*caracteristicasUsadas.size()));
		
		return caracteristica;		
	}
	
	public ArrayList<Caracteristica> obtenerCaracteristicasDificultad(){
		return this.caracteristicasUsadas;
	}
}
