package parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import model.*;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoNula;
import model.Tipo.Tipo;
import model.Tipo.TipoCaracteristica;
import model.Tipo.TipoCiudad;
import model.Tipo.TipoNombre;
import model.Tipo.TipoPasaTiempo;
import model.Tipo.TipoPelo;
import model.Tipo.TipoSexo;
import model.Tipo.TipoVehiculo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class CargarJuego {
	
	private Document generarDoc(String nombreArchivo) throws ParserConfigurationException, SAXException, IOException{
		File archivo = new File(nombreArchivo);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();
		doc = dBuilder.parse(archivo);
		doc.getDocumentElement().normalize();
		return doc;
	}
	
	public Juego cargarJuego(BancoObjetosRobados bancoObjetosRobados, Expediente expediente, Mapa mapa) throws ParserConfigurationException, SAXException, IOException{
		Document doc = generarDoc("Guardable");
		
		
		Tiempo tiempo = this.cargarTiempo((Element)doc.getElementsByTagName("tiempo").item(0));
		Jugador jugador = this.cargarJugadorJ(tiempo, (Element)doc.getElementsByTagName("jugador").item(0), mapa);
		ObjetoRobado objetoRobado = this.cargarObjetoRobadoJ((Element)doc.getElementsByTagName("objeto").item(0), bancoObjetosRobados);
		ArrayList<Pais> recorridoLadronCompleto = this.cargarRecorrido(mapa, (NodeList)doc.getElementsByTagName("recorridoLadronCompleto").item(0).getChildNodes());
		ArrayList<Pais> recorridoLadron = this.cargarRecorrido(mapa,(NodeList)doc.getElementsByTagName("recorridoLadron").item(0).getChildNodes());
		Ladron ladron = this.cargarLadronJ(expediente, (Element)doc.getElementsByTagName("ladron").item(0));
		OrdenDeArresto ordenDeArresto = this.cargarOrdenDeArresto(expediente, (Element)doc.getElementsByTagName("orden").item(0));
		
		
		Mision mision = new Mision();
		try {
			mision.cargarMision(ladron, objetoRobado, bancoObjetosRobados, expediente, mapa,recorridoLadronCompleto, recorridoLadron);
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
		Dificultad dificultad = new Dificultad();

		
		ArrayList<Caracteristica> caract=this.cargarDificultad((NodeList)doc.getElementsByTagName("carateristicasDificultad").item(0).getChildNodes());

		dificultad.setearCaracteristicasUsadas(caract);
		
		jugador.setExpediente(expediente);
		Juego juego = new Juego(jugador, dificultad, mision, tiempo);
		juego.setPaisActual(jugador.paisActual());
		
		dificultad.agregarLadron(ladron);
		
		if (!(ordenDeArresto == null))
			juego.setOrdenDeArresto(ordenDeArresto);
		
		this.cargarPistasPaisAnteriorDelLadron(recorridoLadron.get(0), recorridoLadronCompleto, jugador, dificultad);

		
		return juego;
	}
	
	
	private ArrayList<Caracteristica> cargarDificultad(NodeList pistasDificultad) {

		ArrayList<Caracteristica> caracteristicasDificultad = new ArrayList<Caracteristica>();

		
		for(int i=0; i< (pistasDificultad).getLength(); i++) {
			Tipo tipo = null;
			if(pistasDificultad.item(i).getNodeName()=="caracteristica"){
				String tipoC = ((Element)pistasDificultad.item(i)).getAttribute("tipo");
				if(tipoC.equals("caracteristica")) tipo = new TipoCaracteristica();
				if(tipoC.equals("pasatiempo")) tipo = new TipoPasaTiempo();
				if(tipoC.equals("pelo")) tipo = new TipoPelo();
				if(tipoC.equals("sexo")) tipo = new TipoSexo();
				if(tipoC.equals("vehiculo")) tipo = new TipoVehiculo();
				String descripcion = ((Element)pistasDificultad.item(i)).getAttribute("descripcion");

				Caracteristica caracteristica = new Caracteristica(tipo, descripcion, DificultadGrado.darDificultad("0"));
				caracteristicasDificultad.add(caracteristica);
			}
		}
		return caracteristicasDificultad;
		
	}

	
	private boolean cargarPistasPaisAnteriorDelLadron(Pais paisActualDelLadron,ArrayList<Pais> recorridoLadronCompleto, Jugador jugador, Dificultad dificultad){
		Pais paisAnterior = null;
		for(Pais pais: recorridoLadronCompleto){
			if(pais.equals(paisActualDelLadron))	dificultad.guardarPista(paisAnterior, jugador.obtenerRango());
			paisAnterior = pais;
		}
		return true;
	}


	private Tiempo cargarTiempo(Element elementoTiempo) {
		Tiempo tiempo = new Tiempo(154);
		tiempo.restarHoras(154 - Float.parseFloat(elementoTiempo.getAttribute("tiempo")));
		return tiempo;
	}
	
	private OrdenDeArresto cargarOrdenDeArresto(Expediente expediente, Element elementoOrden) {
		ArrayList <Caracteristica> caracteristicas = new ArrayList<Caracteristica>() ;
		if (!(elementoOrden == null)){
			caracteristicas.add(new Caracteristica(new TipoNombre(), elementoOrden.getAttribute("ladron"), new DificultadGradoNula()));
			
			OrdenDeArresto orden = new OrdenDeArresto(expediente.verCoincidencias(caracteristicas).get(0));
			return orden;
		}
		return null;
	}

	
	private ArrayList<Pais> cargarRecorrido(Mapa mapa, NodeList recorridoLadron){
		
		ArrayList<Pais> recorridoCompleto = new ArrayList<Pais>();
		String pais="";
		
		
		for(int i=0; i< (recorridoLadron).getLength(); i++) {
			if(recorridoLadron.item(i).getNodeName()=="pais"){
				pais= ((Element)recorridoLadron.item(i)).getAttribute("nombre");
				System.out.println(pais);
				recorridoCompleto.add(mapa.obtenerPaisPorCiudad(new Caracteristica(new TipoCiudad(), pais, DificultadGrado.darDificultad("1"))));
			}
		}
		return recorridoCompleto;
		
		
	}
	
	
	private Ladron cargarLadronJ(Expediente expediente, Element elementoLadron){
		
		ArrayList<Caracteristica> caracteristicasDeArchivo = new ArrayList<Caracteristica>();
		caracteristicasDeArchivo.add(new Caracteristica(new TipoNombre(), elementoLadron.getAttribute("nombre"), DificultadGrado.darDificultad("0")));
		ArrayList<Ladron> caracteristicasExpediente = expediente.verCoincidencias(caracteristicasDeArchivo);
		return caracteristicasExpediente.get(0);
	}
	
	public ObjetoRobado cargarObjetoRobadoJ(Element elementoObjetoRobado, BancoObjetosRobados bancoObjetosRobados){
		Caracteristica caracteristica = new Caracteristica(new TipoNombre(), elementoObjetoRobado.getAttribute("nombre"), DificultadGrado.darDificultad("0"));
		try {
			return bancoObjetosRobados.darUnObjetoRobadoConNombre(caracteristica);
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
		return null;
	}
	
	public Jugador cargarJugadorJ(Tiempo tiempo, Element elementoJugador, Mapa mapa ) throws ParserConfigurationException, SAXException, IOException{
		Jugador jugador = new Jugador(tiempo);
		jugador.setPaisActual(mapa.obtenerPaisPorCiudad(new Caracteristica(new TipoCiudad(), elementoJugador.getAttribute("pais"), DificultadGrado.darDificultad("1"))));
		for(int i=0 ; i< Integer.parseInt(elementoJugador.getAttribute("rango")) ; i++) jugador.aumentarArresto();
		return jugador;
	}
	
// CARGA ANTERIOR
//	public Jugador cargarJugador(Mapa mapa) throws ParserConfigurationException, SAXException, IOException{
//		Document doc = generarDoc("Guardable");
//		Element elementoJugador = (Element)doc.getElementsByTagName("jugador").item(0);
//		Tiempo tiempo = new Tiempo(Float.parseFloat(elementoJugador.getAttribute("tiempo")));
//		Jugador jugador = new Jugador(tiempo);
//		Caracteristica caracteristicaPais = new Caracteristica(new TipoCiudad(), elementoJugador.getAttribute("pais"), DificultadGrado.darDificultad("1"));
//		Pais pais = mapa.obtenerPaisPorCiudad(caracteristicaPais);
//		jugador.setPaisActual(pais);
//		for(int i=0 ; i< Integer.parseInt(elementoJugador.getAttribute("rango")) ; i++) jugador.aumentarArresto();
//		return jugador;
//	}
//	
//	public Mision cargarMision(Mapa mapa, Expediente expediente, BancoObjetosRobados objetosRobados) throws ParserConfigurationException, SAXException, IOException{
//		Document doc = generarDoc("Guardable");
//		Element elementoLadron = (Element)doc.getElementsByTagName("ladron").item(0);
//		Element elementoObjeto = (Element)doc.getElementsByTagName("objeto").item(0);
//		Ladron ladron = this.cargarLadron(expediente, elementoLadron.getAttribute("nombre"));
//		Caracteristica caracteristicaObjeto = new Caracteristica(new TipoNombre(), elementoObjeto.getAttribute("nombre"), DificultadGrado.darDificultad("0"));
//		ObjetoRobado objeto = objetosRobados.darUnObjetoRobadoConNombre(caracteristicaObjeto);
//		NodeList recorridoLadron = (NodeList)doc.getElementsByTagName("pais");
//		this.cargarRecorridoLadron(recorridoLadron, ladron, mapa);
//		Mision mision = new Mision();
//		mision.cargarMision(ladron, objeto);
//		return mision;
//	}
//	
//	public OrdenDeArresto cargarOrdenDeArresto(Expediente expediente) throws ParserConfigurationException, SAXException, IOException{
//		Document doc = generarDoc("Guardable");
//		Element elementoSospechoso = (Element)doc.getElementsByTagName("ordenArresto").item(0);
//		String sospechoso = elementoSospechoso.getAttribute("nombre");
//		if(sospechoso.equals("")) return new OrdenDeArresto(null);
//		else{
//			OrdenDeArresto orden = new OrdenDeArresto(this.cargarLadron(expediente, sospechoso));
//			return orden;
//		}
//		
//	}
//
//
//
//	private void cargarRecorridoLadron(NodeList recorridoLadron, Ladron ladron, Mapa mapa) {
//		for(int i=0; i< ((NodeList) recorridoLadron).getLength(); i++) {
//			String pais = ((Element) (recorridoLadron).item(i)).getAttribute("nombre");
//			ladron.agregarRecorrido(mapa.obtenerPaisPorCiudad(new Caracteristica(new TipoCiudad(), pais, DificultadGrado.darDificultad("1"))));			
//		}	
//	}
//
//	
//	private Ladron cargarLadron(Expediente expediente, String nombre) {
//		ArrayList<Caracteristica> caracteristicasDeArchivo = new ArrayList<Caracteristica>();
//		caracteristicasDeArchivo.add(new Caracteristica(new TipoNombre(), nombre, DificultadGrado.darDificultad("0")));
//		ArrayList<Ladron> caracteristicasExpediente = expediente.verCoincidencias(caracteristicasDeArchivo);
//		return caracteristicasExpediente.get(0);
//	}
	

}
