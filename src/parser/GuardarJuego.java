package parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.Caracteristica;
import model.Dificultad;
import model.Juego;
import model.Jugador;
import model.Ladron;
import model.Mision;
import model.ObjetoRobado;
import model.Pais;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.Tipo.TipoNombre;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class GuardarJuego {
	
	
	public boolean guardarJ(Juego juego) throws ParserConfigurationException, TransformerException{
		Jugador jugador = juego.obtenerJugador();
		Mision mision = juego.obtenerMision();
		Ladron ladron = mision.darLadron();
		Dificultad dificultad = juego.obtenerDificultad();
		ObjetoRobado objeto = mision.darObjetoRobado();
		Document doc = generarDoc();
		Element elementoPartida = doc.createElement("partida");
		elementoPartida.appendChild(this.serializarTiempo(doc, jugador));
		elementoPartida.appendChild(this.serializarJugadorJ(doc, jugador));
		elementoPartida.appendChild(this.serializarLadronJ(doc, ladron));
		elementoPartida.appendChild(this.serializarObjetoRobadoJ(doc, objeto ));
		elementoPartida.appendChild(this.serializarRecorridoLadronJ(doc, ladron.obtenerRecorrido()));
		elementoPartida.appendChild(this.serializarCaracteristicasDificultad(doc, dificultad.obtenerCaracteristicasDificultad() ));
		elementoPartida.appendChild(this.serializarRecorridoLadronCompleto(doc, mision.obtenerRecorridoLadronCompleto()));
		if (!juego.getOrdenDeArresto().estaVacia()){
			elementoPartida.appendChild(this.serializarOrdenDeArresto(doc, juego.getOrdenDeArresto().obtenerSospechoso()));
		}
		doc.appendChild(elementoPartida);
		generarXML(doc, "Guardable");
		return true;
	}
	
	private Node serializarTiempo(Document doc, Jugador jugador){
		Element elementoTiempo = doc.createElement("tiempo");
		elementoTiempo.setAttribute("tiempo", Integer.toString(jugador.tiempoRestante()));
		return elementoTiempo;
	}
	
	private Node serializarRecorridoLadronCompleto(Document doc, ArrayList<Pais> RecorridoLadronCompleto) {
		Element elementoRecorridoLadronCompleto = doc.createElement("recorridoLadronCompleto");
		for(Pais pais : RecorridoLadronCompleto){
			Element elementoPais = doc.createElement("pais");
			elementoPais.setAttribute("nombre", pais.toString());
			elementoRecorridoLadronCompleto.appendChild(elementoPais);
		}
		return elementoRecorridoLadronCompleto;
	}

	private Node serializarCaracteristicasDificultad(Document doc,ArrayList<Caracteristica> CaracteristicasDificultad) {
		Element elementoRecorrido = doc.createElement("carateristicasDificultad");
		for(Caracteristica caracteristica : CaracteristicasDificultad){
			Element elementoCaracteristica = doc.createElement("caracteristica");
			elementoCaracteristica.setAttribute("tipo", caracteristica.getTipo().toString());
			elementoCaracteristica.setAttribute("descripcion", caracteristica.getDescripcion());
			elementoRecorrido.appendChild(elementoCaracteristica);
		}
		return elementoRecorrido;
	}

	private Node serializarRecorridoLadronJ(Document doc, Queue<Pais> recorrido) {
		Element elementoRecorrido = doc.createElement("recorridoLadron");
		for(Pais pais : recorrido){
			Element elementoPais = doc.createElement("pais");
			elementoPais.setAttribute("nombre", pais.toString());
			elementoRecorrido.appendChild(elementoPais);
		}
		return elementoRecorrido;
	}

	private Element serializarObjetoRobadoJ(Document doc, ObjetoRobado objeto){
		Element elementoObjeto = doc.createElement("objeto");
		elementoObjeto.setAttribute("nombre", this.obtenerNombreObjeto(objeto));
		return elementoObjeto;	
	}
	
	private Element serializarLadronJ(Document doc, Ladron ladron){
		Element elementoLadron = doc.createElement("ladron");
		elementoLadron.setAttribute("nombre", this.obtenerNombreLadron(ladron));
		return elementoLadron;
	}
	
	private Element serializarJugadorJ(Document doc, Jugador jugador) {
		Element elementoJugador = doc.createElement("jugador");
		elementoJugador.setAttribute("pais", jugador.paisActual().toString());
		elementoJugador.setAttribute("rango", Integer.toString(jugador.cantidadDeArrestos()));
		return elementoJugador;
	}
	
	private Element serializarOrdenDeArresto(Document doc, Ladron ladron) {
		Element elementoOrden = doc.createElement("orden");
		elementoOrden.setAttribute("ladron", this.obtenerNombreLadron(ladron));
		return elementoOrden;
	}

	
	
	private Document generarDoc() throws ParserConfigurationException{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		return doc;
	}
	
	private boolean generarXML(Document doc, String nombreArchivo) throws TransformerException{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		File archivoDestino = new File(nombreArchivo);
		StreamResult result = new StreamResult(archivoDestino);
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(source, result);
		return true;
	}
		
	private String obtenerNombreLadron(Ladron ladron){
		Caracteristica caracteristica = null;
		try {
			caracteristica = ladron.obtenerCaracteristicaDeTipo(new TipoNombre());
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
		return caracteristica.getDescripcion();
	}

	private String obtenerNombreObjeto(ObjetoRobado objeto){
		Caracteristica caracteristica = null;
		try {
			caracteristica = objeto.obtenerCaracteristica(new TipoNombre());
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
		return caracteristica.getDescripcion();
	}
	
}
