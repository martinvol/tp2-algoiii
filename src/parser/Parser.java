package parser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import model.BancoObjetosRobados;
import model.Caracteristica;
import model.Expediente;
import model.Ladron;
import model.Limitrofe;
import model.Mapa;
import model.ObjetoRobado;
import model.Pais;
import model.GradoDificultad.DificultadGrado;
import model.GradoDificultad.DificultadGradoUno;
import model.Tipo.Tipo;
import model.Tipo.TipoAnimales;
import model.Tipo.TipoArte;
import model.Tipo.TipoBandera;
import model.Tipo.TipoCantidadPaises;
import model.Tipo.TipoCaracteristica;
import model.Tipo.TipoCiudad;
import model.Tipo.TipoGente;
import model.Tipo.TipoGeografia;
import model.Tipo.TipoIdioma;
import model.Tipo.TipoIndustria;
import model.Tipo.TipoLider;
import model.Tipo.TipoMisc;
import model.Tipo.TipoMoneda;
import model.Tipo.TipoNombre;
import model.Tipo.TipoPasaTiempo;
import model.Tipo.TipoPelo;
import model.Tipo.TipoPuntosDeInteres;
import model.Tipo.TipoReligion;
import model.Tipo.TipoSexo;
import model.Tipo.TipoVehiculo;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
//import org.w3c.dom.Element;
//import org.w3c.dom.NamedNodeMap;
//import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Parser {
	
	private static final String TIPO_CIUDAD = "ciudad";
	private static final String TIPO_BANDERA = "bandera";
	private static final String TIPO_MONEDA = "moneda";
	private static final String TIPO_GEOGRAFIA = "geografia";
	private static final String TIPO_PUNTOS_REFERENCIA = "puntos_referencia";
	private static final String TIPO_INDUSTRIA = "industria";
	private static final String TIPO_ANIMALES = "animales";
	private static final String TIPO_GENTE  = "gente";
	private static final String TIPO_IDIOMA = "idioma";
	private static final String TIPO_ARTE = "arte";
	private static final String TIPO_RELIGION = "religion";
	private static final String TIPO_LIDER = "lider";
	private static final String TIPO_MISC = "misc";
	private static final String TIPO_NOMBRE = "nombre";
	private static final String TIPO_CANTIDAD_PAISES = "cantidad_paises";
	private static final String TIPO_SEXO = "sexo";
	private static final String TIPO_PASATIEMPO = "pasatiempo";
	private static final String TIPO_PELO = "pelo";
	private static final String TIPO_CARACTERISTICA = "caracteristica";
	private static final String TIPO_VEHICULO = "vehiculo";
	
	public Parser(){};
	
	
	private Document obtenerArchivoNormalizado(String archivo) throws ParserConfigurationException, TransformerException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.newDocument();
		try {
			doc = dBuilder.parse(new File(archivo));
		} catch (SAXException e) {
			e.printStackTrace();
		}
		doc.getDocumentElement().normalize();
		return doc;
	}
	
	
	public Mapa crearMapa(String nombreArchivo) throws ParserConfigurationException, TransformerException, SAXException, IOException{
		Mapa mapa = new Mapa();
		// String nombreArchivo = "resources/datos/paises.xml";
		Document doc = obtenerArchivoNormalizado(nombreArchivo);
		NodeList listaPaises = (NodeList)doc.getElementsByTagName("pais");
		for(int i=0; i< listaPaises.getLength(); i++) {
			NodeList listaCaracteristicas = listaPaises.item(i).getChildNodes();
			mapa.agregarPais(this.hidratar_pais(listaCaracteristicas));
		}
		
		return mapa;	
	}
	
	private Pais hidratar_pais(NodeList listaCaracteristicas) {
		Pais pais = new Pais();
		for(int i=0; i< listaCaracteristicas.getLength(); i++) {
			if(listaCaracteristicas.item(i).getNodeName() == "caracteristica"){ //esto me evita los enters
				Caracteristica caracteristica = this.hidratar_caracteristica(listaCaracteristicas.item(i));
				pais.agregarCaracteristica(caracteristica);	
			}
		}
		return pais;
	}
	
	
	private Caracteristica hidratar_caracteristica(Node caracteristica) {
		String categoria = ((Element)caracteristica).getAttribute("etiqueta");
		Tipo tipo = this.devolverTipo(categoria);
		String descripcion = ((Element)caracteristica).getAttribute("descripcion");
		String dificultad = ((Element)caracteristica).getAttribute("dificultad");
		Caracteristica carac = new Caracteristica(tipo, descripcion, DificultadGrado.darDificultad(dificultad));
		return carac;
	}
	
	private Tipo devolverTipo(String categoria){
		if(categoria.equals(TIPO_BANDERA)) return new TipoBandera();
		if(categoria.equals(TIPO_MONEDA)) return new TipoMoneda();
		if(categoria.equals(TIPO_GEOGRAFIA)) return new TipoGeografia();
		if(categoria.equals(TIPO_PUNTOS_REFERENCIA)) return new TipoPuntosDeInteres();
		if(categoria.equals(TIPO_INDUSTRIA)) return new TipoIndustria();
		if(categoria.equals(TIPO_ANIMALES)) return new TipoAnimales();
		if(categoria.equals(TIPO_GENTE)) return new TipoGente();
		if(categoria.equals(TIPO_IDIOMA)) return new TipoIdioma();
		if(categoria.equals(TIPO_ARTE)) return new TipoArte();
		if(categoria.equals(TIPO_RELIGION)) return new TipoReligion();
		if(categoria.equals(TIPO_LIDER)) return new TipoLider();
		if(categoria.equals(TIPO_MISC)) return new TipoMisc();
		if(categoria.equals(TIPO_CIUDAD)) return new TipoCiudad();
		if(categoria.equals(TIPO_NOMBRE)) return new TipoNombre();
		if(categoria.equals(TIPO_CANTIDAD_PAISES)) return new TipoCantidadPaises();
		if(categoria.equals(TIPO_SEXO)) return new TipoSexo();
		if(categoria.equals(TIPO_PASATIEMPO)) return new TipoPasaTiempo();
		if(categoria.equals(TIPO_PELO)) return new TipoPelo();
		if(categoria.equals(TIPO_CARACTERISTICA)) return new TipoCaracteristica();
		if(categoria.equals(TIPO_VEHICULO)) return new TipoVehiculo();
		
		return null;
	}

	public void agregarLimitrofes(Mapa mapa, String nombreArchivo) throws ParserConfigurationException, TransformerException, SAXException, IOException{
		Document doc = obtenerArchivoNormalizado(nombreArchivo);
		NodeList listaPaises = (NodeList)doc.getElementsByTagName("pais");
		ArrayList<Pais> paises = mapa.getMapa();
		for(int i=0; i< listaPaises.getLength(); i++) { // el i del mapa concide con el i de los limitrofes
			NodeList listaCaracteristicas = listaPaises.item(i).getChildNodes();
			this.hidratarLimitrofes(paises.get(i),listaCaracteristicas, mapa);
		}
		
	}
	
	private void hidratarLimitrofes(Pais paisInicial, NodeList listaCaracteristicas, Mapa mapa) {
		for(int i=0; i< listaCaracteristicas.getLength(); i++) {
			if(listaCaracteristicas.item(i).getNodeName() == "caracteristica"){ 
				Limitrofe limitrofe = hidratarLimitrofe(paisInicial, listaCaracteristicas.item(i), mapa);
				paisInicial.setLimitrofe(limitrofe);
			}
		}
	}
	
	
	private Limitrofe hidratarLimitrofe(Pais paisInicial, Node caracteristica, Mapa mapa) {
		String descripcion = ((Element)caracteristica).getAttribute("ciudad");
		int distancia = Integer.parseInt(((Element)caracteristica).getAttribute("distancia"));
		Caracteristica carac = new Caracteristica(new TipoCiudad(),descripcion,DificultadGradoUno.darDificultad("1"));
		Pais paisDestino = mapa.obtenerPaisPorCiudad(carac);
		Limitrofe limitrofe = new Limitrofe(paisInicial, paisDestino,distancia);
		return limitrofe;
	}
	
	
	public BancoObjetosRobados crearBancoObjetosRobados(String nombreArchivo) throws ParserConfigurationException, TransformerException, SAXException, IOException{
		BancoObjetosRobados banco = new BancoObjetosRobados();
		Document doc = obtenerArchivoNormalizado(nombreArchivo);
		NodeList listaObjetos = (NodeList)doc.getElementsByTagName("objeto");
		for(int i=0; i< listaObjetos.getLength(); i++) {
			NodeList listaCaracteristicas = listaObjetos.item(i).getChildNodes();
			banco.agregarObjetoRobado(this.hidratarObjetoRobado(listaCaracteristicas));
		}
		return banco;	
	}
	
	private ObjetoRobado hidratarObjetoRobado(NodeList listaCaracteristicas){
		ObjetoRobado objeto = new ObjetoRobado();
		for(int i=0; i< listaCaracteristicas.getLength(); i++) {
			if(listaCaracteristicas.item(i).getNodeName() == "caracteristica"){ //esto me evita los enters
				Caracteristica caracteristica = hidratar_caracteristica(listaCaracteristicas.item(i));
				objeto.agregarCaracteristica(caracteristica);
			}
		}
		return objeto;
	}
		
		
	
	public Expediente crearExpediente(String nombreArchivo) throws ParserConfigurationException, TransformerException, SAXException, IOException{
		Expediente expediente = new Expediente();
		Document doc = obtenerArchivoNormalizado(nombreArchivo);
		NodeList listaObjetos = (NodeList)doc.getElementsByTagName("ladron");
		for(int i=0; i< listaObjetos.getLength(); i++) {
			NodeList listaCaracteristicas = listaObjetos.item(i).getChildNodes();
			expediente.agregarLadron(this.hidratarLadron(listaCaracteristicas));
		}
		return expediente;	
		
	}
	
	private Ladron hidratarLadron(NodeList listaCaracteristicas){
		Ladron ladron = new Ladron();
		for(int i=0; i< listaCaracteristicas.getLength(); i++) {
			if(listaCaracteristicas.item(i).getNodeName() == "caracteristica"){ //esto me evita los enters
				Caracteristica caracteristica = hidratar_caracteristica(listaCaracteristicas.item(i));
				ladron.agregarCaracteristica(caracteristica);
			}
		}
		return ladron;
	}
	
	
}
