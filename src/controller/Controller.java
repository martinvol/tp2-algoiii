package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import parser.*;
import model.*;
import parser.CargarJuego;
import parser.GuardarJuego;
import view.Vista;
import model.Caracteristica;
import model.Juego;
import model.Limitrofe;

public class Controller{

	Juego modelo;
	Vista view;

	public Controller() {	
	} 

	public void agregarModelo(Juego modelo){
		this.modelo = modelo;
	}

	public void addView(Vista v){
		
		this.view = v;
	} 

	public void initModel(int x){
			
	} 

	public void viajar(Limitrofe limitrofe) {
		modelo.jugadorViajaA(limitrofe);
	}

	public void buscarLadron(ArrayList<Caracteristica> caracteristicas) {
		
		modelo.generarOrdenDeArresto(caracteristicas);
	}

	public void reiniciarJuego(){
		
    	this.modelo.reiniciar();
				
	}

	public Juego cargar() throws ParserConfigurationException, TransformerException, SAXException, IOException {
		
		Parser parser = new Parser();
		CargarJuego cargaJuego = new CargarJuego();
		Mapa mapa = parser.crearMapa("resources/datos/paises.xml");
		parser.agregarLimitrofes(mapa, "resources/datos/limitrofes.xml");
		Expediente expediente = parser.crearExpediente("resources/datos/ladrones.xml");
		BancoObjetosRobados objetosRobados = parser.crearBancoObjetosRobados("resources/datos/objetos.xml");
		this.modelo = cargaJuego.cargarJuego(objetosRobados, expediente, mapa);
		return modelo;
	}


	public void guardar() throws ParserConfigurationException, TransformerException {
		
		GuardarJuego guardarJuego = new GuardarJuego();
		guardarJuego.guardarJ(modelo);
	}
	
} 