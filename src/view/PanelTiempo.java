package view;

import java.awt.Panel;

import javax.swing.JTextArea;

import model.Juego;

public class PanelTiempo extends Panel{

	private static final long serialVersionUID = 1L;
	private JTextArea texto = new  JTextArea();
	private Vista vista;
	
	 PanelTiempo(Vista vista){
		 
		 this.vista = vista;
		 this.add(this.texto);
		 this.texto.setText("");
		 this.texto.setEditable(false);
		 
	 }
	 
	 public void actualizar(){
		 Juego modelo = vista.getModelo();
		 
		 this.texto.setText(modelo.obtenerTiempo());
	 }
}
