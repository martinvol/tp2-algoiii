package view;

final class Paths {
	String imagenInicial = "resources/imagenes/ladron-dormido-tras-robar-noticia.jpg";
	String imagenBanco = "resources/imagenes/bill.jpg";
	String imagenAeropuerto = "resources/imagenes/aeropuerto.png";
	String imagenBiblioteca = "resources/imagenes/museo.jpg";
	
	public String getPathImagenInicial() {
		
		return this.imagenInicial;
	}

	public String getPathImagenBanco() {
		
		return this.imagenBanco;
	}

	public String getPathImagenAeropuerto() {
		
		return this.imagenAeropuerto;
	}

	public String getPathImagenBiblioteca() {
		
		return this.imagenBiblioteca;
	}
}
