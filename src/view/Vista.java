package view;

import java.awt.Button;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import model.Juego;
import controller.Controller;

public class Vista{

	private JFrame frame;
	private Panel panelActual;
	private PanelInicio panelInicio;
	private PanelPais panelPais;
	private PanelViajar panelViajar;
	private PanelExpediente panelExpediente;
	private PanelPistas panelPistas;
	private PanelTiempo panelTiempo;
	private Juego modelo;
	private Controller controlador;
	
	public Vista(final Controller controlador, Juego juego) {
		
		this.modelo = juego;
		this.controlador = controlador;
		
		frame = new JFrame();
		frame.setTitle("Carmen San Diego");
        frame.setSize(500, 400);
        frame.setLocation(8, 0);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		Button button_guardar = new Button("Guardar");
		Button button_cargar = new Button("Cargar Juego");
        crearPaneles();

		panelActual = panelInicio;
		
		frame.add("North", panelInicio);
		
		Box box = Box.createHorizontalBox();
		box.add(panelTiempo);
		box.add(button_cargar);
		box.add(button_guardar);
		
		frame.add("South", box);
		
        frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH); // Maximiza la ventana
		
		button_cargar.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				try {
					modelo = controlador.cargar();
					cambiarAInicio();
					JOptionPane.showMessageDialog(null, "Juego Cargado", "Info", JOptionPane.INFORMATION_MESSAGE);

				} catch (ParserConfigurationException | TransformerException
						| SAXException | IOException e) {
				
					e.printStackTrace();
				}
        }

		});
		
		button_guardar.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me){
				try {
					controlador.guardar();
					JOptionPane.showMessageDialog(null, "Juego Guardado", "Info", JOptionPane.INFORMATION_MESSAGE);
				} catch (ParserConfigurationException | TransformerException e) {
					
					e.printStackTrace();
				}
        }
		});
		
	}

	private void crearPaneles() {
		
		panelInicio = new PanelInicio(this);
		panelPais =  new PanelPais(this);
		panelViajar = new PanelViajar(this, this.controlador);
		panelExpediente = new PanelExpediente(this, this.controlador);
		panelPistas = new PanelPistas(this);
		panelTiempo = new PanelTiempo(this);
	}


	public void cambiarVista(Panel aPanel) {
		panelTiempo.actualizar();
		frame.getContentPane().remove(panelActual);

		frame.add("North", aPanel);
		
        panelActual = aPanel;
        frame.invalidate();
        frame.validate();
        this.frame.repaint();
        panelActual.invalidate();
        panelActual.validate();
        panelActual.repaint();

        this.panelTiempo.actualizar();

	}
	
	private void cambiarAInicio() {
		this.cambiarVista(this.panelInicio);
		this.panelInicio.actualizar();
		
		
	}
	
	protected void cambiarAPais() {
		this.panelPais.actualizar();
		cambiarVista(panelPais);
		
		boolean termino = false;
		
		if (this.modelo.ladronNoPuedeEscaparseMas()){
			if(modelo.gano()){
		        JOptionPane.showMessageDialog(null, "Arrestaste al ladron buscado!", "Resultado", JOptionPane.INFORMATION_MESSAGE);
		        termino = true;
			} else {
				JOptionPane.showMessageDialog(null, "El ladron esta aca, pero no tenes una orden de arresto que lo capture, podes intentar pedir una!", "Resultado", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		 if (modelo.perdio()) {
			JOptionPane.showMessageDialog(null, "Se te acabo el tiempo, Fin del caso!", "Perdiste", JOptionPane.INFORMATION_MESSAGE);
			termino = true;
		}
		 
		 if (termino) {
			 this.controlador.reiniciarJuego();
			 this.cambiarVista(panelInicio);
			 this.panelInicio.actualizar();
		 }
	}
	
	protected void cambiarViajar() {
        this.panelViajar.actualizar();
		cambiarVista(panelViajar);
	}

	public void cambiarAExpediente() {
		cambiarVista(panelExpediente);
		
	}
	
	private void cambiarALugar(Panel panel) {
		
		cambiarVista(panel);
		int horas = modelo.horasRestadasPorCuchillazos();
		if (horas != 0){
			JOptionPane.showMessageDialog(null, String.format("Fuiste acuchillado, se restan %d horas", horas), "Cuidado!", JOptionPane.INFORMATION_MESSAGE);

		}
	}
	
	public void cambiarABanco() {
		
		this.panelPistas.setPistas(modelo.obtenerJugador().obtenerPistasBanco());
		cambiarALugar(panelPistas);
	}
	
	public void cambiarAAeropuerto() {

		this.panelPistas.setPistas(modelo.obtenerJugador().obtenerPistasAeropuerto());
		cambiarALugar(panelPistas);
	}
	
	public void cambiarAMuseo() {
		
		this.panelPistas.setPistas(modelo.obtenerJugador().obtenerPistasBiblioteca());
		cambiarALugar(panelPistas);
	}

	protected Juego getModelo() {
		
		return this.modelo;
	}
	
}
