package view;

import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import model.Juego;
import model.Excepciones.CaracteristicaNoExistenteException;

public class PanelInicio extends Panel {
	
	private static final long serialVersionUID = 1L;

	private JLabel im;
	private JTextArea texto = new  JTextArea();
	private Vista vista;
	private Paths pathsDeImagenes = new Paths();

	
	public PanelInicio(final Vista vista) {
		
		this.vista = vista;
        im = new JLabel(new ImageIcon(pathsDeImagenes.getPathImagenInicial()));
        
        texto.setEditable(false);

        this.actualizar();
		this.add(texto);
		this.add(im);
		
		im.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarAPais();
        }
		});
		
	}
	
	public void actualizar() {
		Juego modelo = this.vista.getModelo(); 
		texto.setText("");
		texto.append("Empieza el juego \n");
        try {
			texto.append(String.format("El objeto robado fue %s, se trata de un objeto %s\n", modelo.darObjetoRobado().obtenerNombre(), modelo.darObjetoRobado().obtenerDescripcionDelValor()));
		} catch (CaracteristicaNoExistenteException e) {
			System.exit(1);
		}
        texto.append(String.format("Fue robado en %s\n", modelo.obtenerMision().darPaisDeMision().toString()));
        texto.append(String.format("Eres un %s con una cantidad de arrestos %d\n", modelo.obtenerJugador().obtenerRango().obtenerCargo(), modelo.obtenerJugador().cantidadDeArrestos()));
        texto.append("Clic en el ladron para empezar.");
	}
}
