package view;

import java.awt.Button;
import java.awt.Panel;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class PanelLugares extends Panel {

	private static final long serialVersionUID = 1L;
	private Button button; 
	private JLabel im;
	private Paths pathsDeImagenes = new Paths();
	
	public PanelLugares(Vista vista) {
		
        button = new Button("Atras");
        im = new JLabel(new ImageIcon(pathsDeImagenes.getPathImagenBanco()));
		this.add(button);
		this.add(im);
	}
}
