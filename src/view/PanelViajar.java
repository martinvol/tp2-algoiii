package view;

import java.awt.Button;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;

import model.Juego;
import model.Pais;

import controller.Controller;
import model.Limitrofe;;

public class PanelViajar extends Panel {

	private static final long serialVersionUID = 1L;

	JButton button1, button2, button3, button4;

	private Button buttonAtras, buttonExpediente; 
	private Vista vista;
	private Controller controlador;
	
	PanelViajar(final Vista vista, final Controller controlador){
		
		this.vista = vista;
		this.controlador = controlador;
		
    	new Pais();
    	new Pais();
    	new Pais();

		button1 = new JButton();  
        button2	= new JButton();
        button3 = new JButton(); 
        button4	= new JButton(); 
        	        
        buttonAtras = new Button("Atras");
        buttonExpediente = new Button("Expediente");
		
		this.add(button1);
		this.add(button2);
		this.add(button3);
		this.add(button4);
		this.add(buttonAtras);
		
		this.actualizar();
			
		buttonAtras.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me){
			vista.cambiarAPais();
        }
		});
		
		
		buttonExpediente.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me){
			vista.cambiarAExpediente();
        }
		});	
	}
	
	public void actualizar(){
		
		Juego modelo = this.vista.getModelo();
		final ArrayList<Limitrofe> limitrofes = modelo.paisActual().getLimitrofes();
	
		button1.setText(limitrofes.get(0).obtenerDestino().toString());
		button2.setText(limitrofes.get(1).obtenerDestino().toString());
		button3.setText(limitrofes.get(2).obtenerDestino().toString());
		
		if (limitrofes.size() > 3) {
			button4.setVisible(true);
			button4.setText(limitrofes.get(3).obtenerDestino().toString());
		} else 
			button4.setVisible(false);

		for( MouseListener al : button1.getMouseListeners()) {
			button1.removeMouseListener( al );
	    }
		for( MouseListener al : button2.getMouseListeners()) {
			button2.removeMouseListener( al );
	    }
		for( MouseListener al : button3.getMouseListeners()) {
			button3.removeMouseListener( al );
	    }
		for( MouseListener al : button4.getMouseListeners()) {
			button4.removeMouseListener( al );
	    }		
		
		button1.addMouseListener(new MouseAdapter() {
			
			public void mousePressed(MouseEvent me) {
			controlador.viajar(limitrofes.get(0));
			vista.cambiarAPais();
        }
		});

		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				controlador.viajar(limitrofes.get(1));
				vista.cambiarAPais();
        }
		});
		
		button3.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				controlador.viajar(limitrofes.get(2));
				vista.cambiarAPais();
        }
    });

		button4.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me){
				if (limitrofes.size() > 3){
					controlador.viajar(limitrofes.get(3));
				}
				vista.cambiarAPais();
        }
		});
	}	
}