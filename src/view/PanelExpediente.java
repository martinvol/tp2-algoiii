package view;

import java.awt.Button;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import model.Caracteristica;
import model.GradoDificultad.DificultadGradoNula;
import model.Tipo.*;

import controller.Controller;

public class PanelExpediente extends Panel {

	private static final long serialVersionUID = 1L;

	private ArrayList<CaracteristicasComboBox> comboBoxes;
	
	PanelExpediente(final Vista vista, final Controller controlador){
		
		JTextArea texto = new JTextArea();
		
		texto.append("Este va a ser el expediente");
		Button button_atras = new Button("Atras");
		Button button_aceptar = new Button("Buscar");
		
		String[] sexos_nombres = {"Femenino", "Masculino"}; // hay que ver donde poner estas listas
		String[] hobbies_nombres = {"Tenis", "Musica", "Alpinismo", "Paracaidismo", "Natacion", "Croquet"};		
		
		ArrayList <Caracteristica> sexos = generarListaCaracteristica(new TipoSexo(), sexos_nombres);
		
		ArrayList <Caracteristica> hobbies = generarListaCaracteristica(new TipoPasaTiempo(), hobbies_nombres);
			
		String[] pelo_textos = {"Castanio", "Rubio", "Rojo", "Negro", "Marron"};
		ArrayList <Caracteristica> pelo = generarListaCaracteristica(new TipoPelo(), pelo_textos);

		String[] senia_textos = {"Anillo", "Tatuaje", "Cicatriz", "Collar de joyas"};
		ArrayList <Caracteristica> senia = generarListaCaracteristica(new TipoCaracteristica(), senia_textos);

		String[] vehiculo_textos = {"Descapotable", "Limosina", "Deportivo", "Motocicleta", "Convertible"};
		ArrayList <Caracteristica> vehiculo = generarListaCaracteristica(new TipoVehiculo(), vehiculo_textos);

		this.comboBoxes = new ArrayList<CaracteristicasComboBox>();
		
		comboBoxes.add(new CaracteristicasComboBox("Sexo", sexos));
		comboBoxes.add(new CaracteristicasComboBox("Hobbie", hobbies));
		comboBoxes.add(new CaracteristicasComboBox("Pelo", pelo));
		comboBoxes.add(new CaracteristicasComboBox("Senia", senia));
		comboBoxes.add(new CaracteristicasComboBox("Vehiculo", vehiculo));
		
		Box box = Box.createVerticalBox();
		for (CaracteristicasComboBox combo: comboBoxes) {
			Box box2 = Box.createHorizontalBox();
			box2.add(new JLabel(combo.getNombre()));
			box2.add(combo);
			box.add(box2);
		}
		
		box.add(button_aceptar);		
		box.add(button_atras);
		
		this.add(box);

		
		button_atras.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarAPais();
		}
		});

		button_aceptar.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				controlador.buscarLadron(collectarCaracteristicas());
				if (vista.getModelo().ordenDeArrestoVacia()) {
					JOptionPane.showMessageDialog(null, "No se encontro a un ladron con esas caracteristicas", "Orden", JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Orden de arresto emitida!", "Orden", JOptionPane.INFORMATION_MESSAGE);
					vista.cambiarAPais();
				}
        }

    });
		
	}

	private ArrayList <Caracteristica> collectarCaracteristicas() {
		
		ArrayList <Caracteristica> salida = new ArrayList<Caracteristica>();
		for(CaracteristicasComboBox combo: this.comboBoxes){
			if (!(combo.obtenerCaracteristica() == null)){
			salida.add(combo.obtenerCaracteristica());
			}
		}
		
		return salida;
	}

	private ArrayList <Caracteristica> generarListaCaracteristica(Tipo tipo, String[] nombres) {
		
		ArrayList <Caracteristica> salida = new ArrayList<Caracteristica>();
		for (String nombre: nombres)
			salida.add(new Caracteristica(tipo, nombre, new DificultadGradoNula()));
		return salida;
	}
}
