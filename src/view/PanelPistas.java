package view;

import java.awt.Button;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTextArea;

import model.Caracteristica;

public class PanelPistas extends Panel {

	private static final long serialVersionUID = 1L;

	private JTextArea texto;
	
	PanelPistas(final Vista vista){
		texto = new  JTextArea();
		texto.setEditable(false);
		
		Button button_atras = new Button("Atras");
		this.add(texto);
		this.add(button_atras);
		
		button_atras.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me){
			vista.cambiarAPais();
        }
    });
	
	}

	public void setPistas(ArrayList<Caracteristica> caracteristicas) {
		this.texto.setText("");
		
		for (Caracteristica caracteristica: caracteristicas) { 
			this.texto.append(caracteristica.mostrarComoPista() + '\n');
		}
	}
}
