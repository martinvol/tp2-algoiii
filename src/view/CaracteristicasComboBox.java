package view;

import java.util.ArrayList;

import javax.swing.JComboBox;

import model.Caracteristica;

public class CaracteristicasComboBox extends JComboBox<Object> {

	private static final long serialVersionUID = 1L;
	private ArrayList <Caracteristica> caracteristicas;
	private String nombre;

	public CaracteristicasComboBox(String string, ArrayList <Caracteristica> caracteristicas) {
		
		this.nombre = string;
		this.caracteristicas = caracteristicas;
		this.setSelectedItem(0);
		
		// primera categoria en blanco
		this.addItem(new ComboItem("" , null));
		
		for(Caracteristica caracteristica: this.caracteristicas){
			this.addItem(new ComboItem(caracteristica.getDescripcion(), caracteristica));	
		}		
	}
	
	public Caracteristica obtenerCaracteristica() {
		
		return ((ComboItem)this.getSelectedItem()).getValue();
	}

	public String getNombre() {
		
		return this.nombre;
	}
}

class ComboItem {
	
    private String key;
    private Caracteristica value;

    public ComboItem(String key, Caracteristica caracteristica) {
        this.key = key;
        this.value = caracteristica;
    }

    @Override
    public String toString(){
        return key;
    }

    public String getKey(){
        return key;
    }

    public Caracteristica getValue(){
        return value;
    }
}
