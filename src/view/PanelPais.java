package view;

import java.awt.Button;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import model.Juego;

public class PanelPais extends Panel{

	private static final long serialVersionUID = 1L;
	private Button button; 
	private JLabel imagenBanco, imagenAeropuerto, imagenMuseo, paisActual;
	private Vista vista;
	private Paths pathsDeImagenes = new Paths();
	
	PanelPais(final Vista vista) {
		this.vista = vista;
		
        button = new Button("Viajar");
        Button boton_expediente	= new Button("expediente");

        
        
        Box boxGeneral = Box.createHorizontalBox();
        
		Box boxBanco = Box.createVerticalBox();
		
		imagenBanco = new JLabel(new ImageIcon(pathsDeImagenes.getPathImagenBanco()));
		boxBanco.add(imagenBanco);
		boxBanco.add(new JLabel("Banco"));
		
		Box boxAeropuerto = Box.createVerticalBox();
        imagenAeropuerto = new JLabel(new ImageIcon(pathsDeImagenes.getPathImagenAeropuerto()));
		boxAeropuerto.add(imagenAeropuerto);
		boxAeropuerto.add(new JLabel("Aeropuerto"));

		Box boxMuseo= Box.createVerticalBox();
        imagenMuseo = new JLabel(new ImageIcon(pathsDeImagenes.getPathImagenBiblioteca()));
        boxMuseo.add(imagenMuseo);
        boxMuseo.add(new JLabel("Museo"));
		
      
		
		boxGeneral.add(boxBanco);
		boxGeneral.add(boxAeropuerto);
		boxGeneral.add(boxMuseo);
		
		paisActual = new JLabel();
		this.add(boxGeneral);
		this.add(button);
		this.add(boton_expediente);
		this.add(paisActual);		
		
		this.actualizar();
		
		button.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarViajar();
        }
		});
		
		boton_expediente.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarAExpediente();
        }
		});
		
		imagenBanco.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarABanco();
        }
		});
		
		imagenAeropuerto.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarAAeropuerto();
        }
		});
		
		imagenMuseo.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
			vista.cambiarAMuseo();
        }
		});
	}


	public void actualizar() {
		Juego modelo = this.vista.getModelo();
		this.paisActual.setText(modelo.paisActual().toString());
	}	
}
