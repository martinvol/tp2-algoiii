package integracion;

import static org.junit.Assert.*;

import java.util.*;

import model.*;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.*;
import model.Tipo.*;

import org.junit.Before;
import org.junit.Test;

public class integracion {

	private static final int TIEMPO_ORDEN_DE_ARRESTO = 3;
	private static final int CANT_INIT_ARRESTOS = 0;
	private static final int TIEMPO_INGRESO_1RA_VEZ = 1;
	private static final int TIEMPO_INGRESO_2DA_VEZ = 2;
	private static final int TIEMPO_INGRESO_3RA_VEZ = 3;
	private int MAX_TIEMPO = 24 * 6 + 10;
	private Jugador unJugador;
	private Tiempo unTiempo;
	private Mision unaMision;
	private Dificultad unaDificultad;
	private Expediente unExpediente;
	private Ladron sospechoso, otroLadron, otroLadronMas;
	private ArrayList<Caracteristica> caracteristicasDelSospechoso = new ArrayList<Caracteristica>();
	private ArrayList<Caracteristica> caracteristicasDeOtroLadron = new ArrayList<Caracteristica>();
	private Pais unPaisInicial;
	private DificultadGrado dif0,dif1,dif2,dif3,dif4;
	private Mapa unMapa;
	private Pais bagdad;
	private Limitrofe conexionInicialBagdad;
	private Pais bamako;
	private Limitrofe conexionInicialBamako;
	private Pais bangkok;
	private Limitrofe conexionInicialBangkok;
	private Pais sydney;
	private Limitrofe conexionInicialSydney;
	private BancoObjetosRobados unBancoDeObjetosRobados;
	private ObjetoRobado unObjetoRobado;
	private Tipo tipoNombre, tipoCiudad, tipoCantidadPaises,tipoBandera,tipoGeografia,tipoMoneda,tipoIndustria,tipoGente,tipoIdioma,tipoLider,tipoCaracteristica;
	private Caracteristica caracteristicaObjRobado1;
	private Caracteristica caracteristicaObjRobado2;
	private Caracteristica caracteristicaObjRobado3;
	private Limitrofe conexionBamakoBangkok;
	private Limitrofe conexionBamakoSydney;
	private Limitrofe conexionBamakoBagdad;
	private Limitrofe conexionBagdadSydney;
	private Limitrofe conexionBagdadBangkok;
	private Limitrofe conexionBangkokSydney;
	private Caracteristica peloDelSospechoso,seniaDelSospechoso,vehiculoDelSospechoso,otraCaracteristica,nombreCiudad,colorBandera,moneda,puntoGeografico1,puntoGeografico2,
	industria1,industria2,gente1,gente2,idioma,lider,misc1,misc2,nombreCiudadBagdad,nombreCiudadBamako,nombreCiudadBangkok,nombreCiudadSydney;

	
	@Before
	public void setUp() throws Exception {
		
		tipoCiudad = new TipoCiudad();
		tipoBandera = new TipoBandera();
		tipoGeografia= new TipoGeografia();
		tipoMoneda= new TipoMoneda();
		tipoIndustria = new TipoIndustria();
		tipoGente = new TipoGente();
		tipoIdioma = new TipoIdioma();
		tipoLider= new TipoLider();
		tipoCaracteristica = new TipoCaracteristica();
		
		unTiempo = new Tiempo(MAX_TIEMPO);
		unJugador = new Jugador(unTiempo);
		unaMision = new Mision();
		unaDificultad = new Dificultad();
		unExpediente = new Expediente();
		
		dif0 = DificultadGrado.darDificultad("0");
		dif1 = DificultadGrado.darDificultad("1");
		dif2 = DificultadGrado.darDificultad("2");
		dif3 = DificultadGrado.darDificultad("3");
		dif4 = DificultadGrado.darDificultad("4");
		
		sospechoso = new Ladron();
		peloDelSospechoso = new Caracteristica(new TipoPelo(), "Negro", dif0);
		sospechoso.agregarCaracteristica(peloDelSospechoso);
		caracteristicasDelSospechoso.add(peloDelSospechoso);
		seniaDelSospechoso = new Caracteristica(new TipoCaracteristica(), "Tatuaje", dif0);
		sospechoso.agregarCaracteristica(seniaDelSospechoso);
		caracteristicasDelSospechoso.add(seniaDelSospechoso);
		vehiculoDelSospechoso = new Caracteristica(new TipoVehiculo(), "Limusina", dif0);
		sospechoso.agregarCaracteristica(vehiculoDelSospechoso);
		caracteristicasDelSospechoso.add(vehiculoDelSospechoso);
		
		unExpediente.agregarLadron(sospechoso);
		
		otroLadron = new Ladron();
		otroLadronMas = new Ladron();
		
		unExpediente.agregarLadron(otroLadron);
		unExpediente.agregarLadron(otroLadronMas);
		
		otraCaracteristica = new Caracteristica(new TipoSexo(), "Femenino", dif0);
		caracteristicasDeOtroLadron.add(otraCaracteristica);
		unJugador.setExpediente(unExpediente);
		
		unPaisInicial = new Pais();
		
		// Ahora empiezo agregando info de los XML
		nombreCiudad = new Caracteristica(tipoCiudad, "Atenas", dif0);
		unPaisInicial.agregarCaracteristica(nombreCiudad);
		
		colorBandera = new Caracteristica(tipoBandera, "Azul y Blanca", dif1);
		moneda = new Caracteristica(tipoMoneda, "Dracma", dif2);
		puntoGeografico1 = new Caracteristica(tipoGeografia, "Mar Egeo", dif2);
		puntoGeografico2 = new Caracteristica(tipoGeografia, "montanias Pindos", dif2);
		industria1 = new Caracteristica(tipoIndustria, "Higo", dif4);
		industria2 = new Caracteristica(tipoIndustria, "Olivo", dif4);
		gente1 = new Caracteristica(tipoGente, "Platon", dif3);
		gente2 = new Caracteristica(tipoGente, "Espartanos", dif3);
		idioma = new Caracteristica(tipoIdioma, "Griego", dif2);
		lider = new Caracteristica(tipoLider, "Primer Ministro", dif3);
		misc1 = new Caracteristica(tipoCaracteristica, "Republica Helenica", dif4);
		misc2 = new Caracteristica(tipoCaracteristica, "Vecinos de Yugoslavia", dif4);
		
		unPaisInicial.agregarCaracteristica(colorBandera);
		unPaisInicial.agregarCaracteristica(moneda);
		unPaisInicial.agregarCaracteristica(puntoGeografico1);
		unPaisInicial.agregarCaracteristica(puntoGeografico2);
		unPaisInicial.agregarCaracteristica(industria1);
		unPaisInicial.agregarCaracteristica(industria2);
		unPaisInicial.agregarCaracteristica(gente1);
		unPaisInicial.agregarCaracteristica(gente2);
		unPaisInicial.agregarCaracteristica(idioma);
		unPaisInicial.agregarCaracteristica(lider);
		unPaisInicial.agregarCaracteristica(misc1);
		unPaisInicial.agregarCaracteristica(misc2);
		
		// Por simplicidad de nombres de variables pongo a los paises como nombres de ciudades.
		bagdad = new Pais();
		bamako = new Pais();
		bangkok = new Pais();
		sydney = new Pais();
		
		nombreCiudadBagdad = new Caracteristica(tipoCiudad, "Bagdad", dif1);
		nombreCiudadBamako = new Caracteristica(tipoCiudad, "Bamako", dif1);
		nombreCiudadBangkok = new Caracteristica(tipoCiudad, "Bangkok", dif1);
		nombreCiudadSydney = new Caracteristica(tipoCiudad, "Sydney", dif1);
		bagdad.agregarCaracteristica(nombreCiudadBagdad);
		bamako.agregarCaracteristica(nombreCiudadBamako);
		bangkok.agregarCaracteristica(nombreCiudadBangkok);
		sydney.agregarCaracteristica(nombreCiudadSydney);
		
		conexionInicialBagdad = new Limitrofe(unPaisInicial, bagdad, 1934);
		conexionInicialBamako = new Limitrofe(unPaisInicial, bamako, 4213);
		conexionInicialBangkok = new Limitrofe(unPaisInicial, bangkok, 7922);
		conexionInicialSydney = new Limitrofe(unPaisInicial, sydney, 15326);
		
		conexionBamakoBangkok = new Limitrofe(bamako, bangkok, 0);
		conexionBamakoSydney = new Limitrofe(bamako, sydney, 0);
		conexionBamakoBagdad = new Limitrofe(bamako, bagdad, 0);
		
		conexionBagdadSydney = new Limitrofe(bagdad, sydney, 0);
		conexionBagdadBangkok= new Limitrofe(bagdad, bangkok, 0);
		
		conexionBangkokSydney=new Limitrofe(bangkok, sydney, 0);
		
		unPaisInicial.setLimitrofe(conexionInicialBagdad);
		unPaisInicial.setLimitrofe(conexionInicialBamako);
		unPaisInicial.setLimitrofe(conexionInicialBangkok);
		unPaisInicial.setLimitrofe(conexionInicialSydney);
		
		bamako.setLimitrofe(conexionInicialBamako);
		bamako.setLimitrofe(conexionBamakoBangkok);
		bamako.setLimitrofe(conexionBamakoSydney);
		bamako.setLimitrofe(conexionBamakoBagdad);
		
		bagdad.setLimitrofe(conexionBamakoBagdad);
		bagdad.setLimitrofe(conexionInicialBagdad);
		bagdad.setLimitrofe(conexionBagdadSydney);
		bagdad.setLimitrofe(conexionBagdadBangkok);
		
		bangkok.setLimitrofe(conexionBagdadBangkok);
		bangkok.setLimitrofe(conexionInicialBangkok);
		bangkok.setLimitrofe(conexionBamakoBangkok);
		bangkok.setLimitrofe(conexionBangkokSydney);
		
		sydney.setLimitrofe(conexionBagdadSydney);
		sydney.setLimitrofe(conexionBamakoSydney);
		sydney.setLimitrofe(conexionBangkokSydney);
		sydney.setLimitrofe(conexionInicialSydney);
		
		unJugador.setPaisActual(unPaisInicial);
		
		unMapa = new Mapa();
		unMapa.agregarPais(unPaisInicial);
		unMapa.agregarPais(bagdad);
		unMapa.agregarPais(bamako);
		unMapa.agregarPais(bangkok);
		unMapa.agregarPais(sydney);
		
		unBancoDeObjetosRobados = new BancoObjetosRobados();
		unObjetoRobado = new ObjetoRobado();
		
		tipoNombre = new TipoNombre();
		
		tipoCantidadPaises = new TipoCantidadPaises();
		
		caracteristicaObjRobado1 = new Caracteristica(tipoNombre, "Lampara de Aladino", dif0);
		caracteristicaObjRobado2 = nombreCiudad;
		caracteristicaObjRobado3 = new Caracteristica(tipoCantidadPaises, "3", dif0);
		
		unObjetoRobado.agregarCaracteristica(caracteristicaObjRobado1);
		unObjetoRobado.agregarCaracteristica(caracteristicaObjRobado2);
		unObjetoRobado.agregarCaracteristica(caracteristicaObjRobado3);
		
		unBancoDeObjetosRobados.agregarObjetoRobado(unObjetoRobado);
		
		unaMision.agregarBancoObjetosRobados(unBancoDeObjetosRobados);
		unaMision.agregarExpedientes(unExpediente);
		unaMision.agregarMapa(unMapa);		
	}

	@Test
	public void testUnJugadorDeberiaPoderViajarDesdeUnPaisASusLimitrofes() {

		ArrayList<Limitrofe> limitrofes = unPaisInicial.getLimitrofes();
		Limitrofe unLimitrofe = limitrofes.get(0);
		unJugador.viajarA(unLimitrofe);
		
		assertEquals(unLimitrofe.obtenerLimitrofeDesde(unPaisInicial), unJugador.paisActual());
		
		int tiempoViaje = (int) unJugador.obtenerRango().obtenerTiempoDeViaje(unLimitrofe.obtenerDistancia());
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - tiempoViaje);
		
		// Ahora testeamos la vuelta al pais inicial
		unJugador.viajarA(unLimitrofe);
		
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - 2*tiempoViaje);
		assertEquals(unJugador.paisActual(), unPaisInicial);
	}
	
	
	@Test
	public void testUnLadronDeberiaRecorrerSuItinerarioCorrectamente() {
		
		Pais pais1 = new Pais();
		Caracteristica nombre1 = new Caracteristica(new TipoCiudad(), "PaisA", new DificultadGradoNula());
		pais1.agregarCaracteristica(nombre1);
		
		Pais pais2 = new Pais();
		Caracteristica nombre2 = new Caracteristica(new TipoCiudad(), "PaisB", new DificultadGradoNula());
		pais2.agregarCaracteristica(nombre2);
		
		Pais pais3 = new Pais();
		Caracteristica nombre3 = new Caracteristica(new TipoCiudad(), "PaisC", new DificultadGradoNula());
		pais3.agregarCaracteristica(nombre3);
		
		Ladron ladron = new Ladron();
		
		ladron.agregarRecorrido(pais1);
		ladron.agregarRecorrido(pais2);
		ladron.agregarRecorrido(pais3);
			
		assertEquals(ladron.obtenerPaisActual(), pais1);
		assertTrue(ladron.viajar());
		
		assertEquals(ladron.obtenerPaisActual(), pais2);
		assertTrue(ladron.viajar());
		
		assertEquals(ladron.obtenerPaisActual(), pais3);
		assertFalse(ladron.viajar());
	}
	
	@Test
	public void testUnExpedienteDeberiaDevolverCorrectamenteTodasLasCoincidenciasAPartirDeUnaCaracteristica() {
		
		// Creo algunos ladrones combinando 4 caracteristicas de a pares (Pelo y Senia)
		Expediente expediente = new Expediente();
		Caracteristica colordePeloBuscado = new Caracteristica(new TipoPelo(), "Rojo", new DificultadGradoNula());
		Caracteristica otraCaracteristica = new Caracteristica(new TipoPelo(), "Azul", new DificultadGradoNula());
		Caracteristica tercerCaracteristica = new Caracteristica(new TipoCaracteristica(), "Anillo", new DificultadGradoNula());
		Caracteristica seniaBuscada = new Caracteristica(new TipoCaracteristica(), "Tatuaje", new DificultadGradoNula());
		
		Ladron ladron1 = new Ladron();
		ladron1.agregarCaracteristica(colordePeloBuscado);
		ladron1.agregarCaracteristica(tercerCaracteristica);
		
		Ladron ladron2 = new Ladron();
		ladron2.agregarCaracteristica(otraCaracteristica);
		ladron2.agregarCaracteristica(tercerCaracteristica);
		
		Ladron ladron3 = new Ladron();
		ladron3.agregarCaracteristica(colordePeloBuscado);
		ladron3.agregarCaracteristica(seniaBuscada);
		
		Ladron ladron4 = new Ladron();
		ladron4.agregarCaracteristica(otraCaracteristica);
		ladron4.agregarCaracteristica(seniaBuscada);
		
		ArrayList<Caracteristica> caracteristicasLadron1 = new ArrayList<Caracteristica>();
		caracteristicasLadron1.add(colordePeloBuscado);
		caracteristicasLadron1.add(tercerCaracteristica);
		
		ArrayList<Caracteristica> caracteristicasLadron4 = new ArrayList<Caracteristica>();
		caracteristicasLadron4.add(otraCaracteristica);
		caracteristicasLadron4.add(seniaBuscada);
		
		assertTrue(expediente.agregarLadron(ladron1));
		assertTrue(expediente.agregarLadron(ladron2));
		assertTrue(expediente.agregarLadron(ladron3));
		assertTrue(expediente.agregarLadron(ladron4));
		
		assertTrue(expediente.verCoincidencias(caracteristicasLadron1).contains(ladron1));
		assertFalse(expediente.verCoincidencias(caracteristicasLadron1).contains(ladron2));
		assertFalse(expediente.verCoincidencias(caracteristicasLadron4).contains(ladron3));
		assertTrue(expediente.verCoincidencias(caracteristicasLadron4).contains(ladron4));
		
		// Ahora vemos coincidencias de dos caracteristicas. En total debemos tener dos coincidencias.
		caracteristicasLadron1.remove(tercerCaracteristica);
		caracteristicasLadron4.remove(otraCaracteristica);
		
		assertTrue(expediente.verCoincidencias(caracteristicasLadron1).contains(ladron1));
		assertTrue(expediente.verCoincidencias(caracteristicasLadron1).contains(ladron3));
		
		assertTrue(expediente.verCoincidencias(caracteristicasLadron4).contains(ladron4));
		assertTrue(expediente.verCoincidencias(caracteristicasLadron4).contains(ladron3));
	}
	
	@Test
	public void testUnJugadorNovatoArrestaAUnLadron() {
		
		OrdenDeArresto ordenInvalida = unJugador.generarOrdenDeArresto(caracteristicasDeOtroLadron);
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - TIEMPO_ORDEN_DE_ARRESTO);
		assertFalse(ordenInvalida.correspondeA(sospechoso));
		assertFalse(unJugador.arrestarConUnaOrden(ordenInvalida, sospechoso));
		
		OrdenDeArresto orden = unJugador.generarOrdenDeArresto(caracteristicasDelSospechoso);
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - 2*TIEMPO_ORDEN_DE_ARRESTO);
		assertTrue(orden.correspondeA(sospechoso));
		assertTrue(unJugador.arrestarConUnaOrden(orden, sospechoso));
		
		assertEquals(unJugador.cantidadDeArrestos(), CANT_INIT_ARRESTOS + 1);
	}
	
	@Test
	public void testElJuegoDeberiaControlarQueElLadronViajeCuandoSeEncuentraEnElMismoPaisQueElJugador() {
		
		Juego unJuego = new Juego(unJugador, unaDificultad, unaMision, unTiempo);
	
		try {
			unaMision.nuevaMision();
		} catch (CaracteristicaNoExistenteException e) {}
		
		unJuego.inicializarNivel();
		
		Ladron ladron = unJuego.getLadron();
		assertFalse(ladron.obtenerPaisActual() == unJugador.paisActual());
		Limitrofe unLimitrofe = new Limitrofe(ladron.obtenerPaisActual(), unJugador.paisActual(), 0);
		unJuego.jugadorViajaA(unLimitrofe);
		
		assertFalse(ladron.obtenerPaisActual() == unJugador.paisActual());		
	}
	
	@Test 
	public void testUnJugadorNovatoObtienePistasDeLosLugaresDeLosPaisesYSeLeRestaElTiempo() {
		
		unJugador.setPaisActual(unPaisInicial);
		
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO);
		
		unJugador.obtenerPistasBiblioteca().get(0);
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - TIEMPO_INGRESO_1RA_VEZ);
		
		unJugador.obtenerPistasAeropuerto().get(0);
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - TIEMPO_INGRESO_1RA_VEZ - TIEMPO_INGRESO_2DA_VEZ);
		
		unJugador.obtenerPistasBanco().get(0);
		assertEquals(unJugador.tiempoRestante(), MAX_TIEMPO - TIEMPO_INGRESO_1RA_VEZ - TIEMPO_INGRESO_2DA_VEZ - TIEMPO_INGRESO_3RA_VEZ);
		
		assertEquals(unTiempo.toString(), "Lunes, 13 hs");
		
		int horasRestantes = MAX_TIEMPO - TIEMPO_INGRESO_1RA_VEZ - TIEMPO_INGRESO_2DA_VEZ - TIEMPO_INGRESO_3RA_VEZ;
		
		unJugador.viajarA(conexionInicialBagdad);
		int tiempoViaje = (int) unJugador.obtenerRango().obtenerTiempoDeViaje(conexionInicialBagdad.obtenerDistancia());
		assertEquals(unJugador.tiempoRestante(), horasRestantes - tiempoViaje);	 
	}
	
	@Test
	public void testUnJugadorNovatoCapturaAUnLadronEnElJuego() {
		
		Expediente expedienteDeUnSoloLadron = new Expediente();
		expedienteDeUnSoloLadron.agregarLadron(sospechoso);
		unaMision.agregarExpedientes(expedienteDeUnSoloLadron);
		Juego unJuego = new Juego(unJugador, unaDificultad, unaMision, unTiempo);

		try {
			unaMision.nuevaMision();
		} catch (CaracteristicaNoExistenteException e) {}
		
		unJuego.inicializarNivel();
		
		Ladron ladron = unJuego.getLadron();
		unJuego.generarOrdenDeArresto(caracteristicasDelSospechoso);
		
		assertFalse(unJuego.gano());
		assertFalse(unJuego.perdio());
		assertFalse(unJuego.terminoElNivel());
		
		Limitrofe unLimitrofe = new Limitrofe(ladron.obtenerPaisActual(), unJugador.paisActual(), 0);
		unJuego.jugadorViajaA(unLimitrofe);
		unLimitrofe = new Limitrofe(ladron.obtenerPaisActual(), unJugador.paisActual(), 0);
		unJuego.jugadorViajaA(unLimitrofe);
		
		assertTrue(unJuego.gano());
		assertEquals(unJuego.obtenerJugador().cantidadDeArrestos(), 1);
	}
	
	@Test
	public void testElJuegoDeberiaImprimirsuTiempoCorrectamente(){
		Juego unJuego = new Juego(unJugador, unaDificultad, unaMision, unTiempo);
		assertEquals(unJuego.obtenerTiempo(), "Lunes, 7 hs");
	}
	
	@Test
	public void testUnPaisSabeImprimirseCorrectamente() {
		
		assertEquals(unPaisInicial.toString(), "Atenas");
	}
	
	@Test
	public void unaMisionDeberiaArmarUnRecorridoAunqueLaCantidadDePaisesNoSeaSuficiente() {
		
		Mapa otroMapa = new Mapa();
		otroMapa.agregarPais(bagdad);
		otroMapa.agregarPais(unPaisInicial);
		
		ObjetoRobado otroObjetoRobado = new ObjetoRobado();
		otroObjetoRobado.agregarCaracteristica(new Caracteristica(new TipoCantidadPaises(), "3", dif0));
		otroObjetoRobado.agregarCaracteristica(new Caracteristica(new TipoCiudad(), "Atenas", dif0));
		
		BancoObjetosRobados otroBanco = new BancoObjetosRobados();
		otroBanco.agregarObjetoRobado(otroObjetoRobado);
		
		unaMision.agregarBancoObjetosRobados(otroBanco);
		unaMision.agregarMapa(otroMapa);
		
		try {
			assertTrue(unaMision.nuevaMision());
		} catch (CaracteristicaNoExistenteException e) {}
	}
}
