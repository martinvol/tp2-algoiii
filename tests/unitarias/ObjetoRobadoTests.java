package unitarias;

import static org.junit.Assert.*;
import model.Caracteristica;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;
import model.ObjetoRobado;

import org.junit.Before;
import org.junit.Test;

public class ObjetoRobadoTests {

	private static final String DIFICULTAD_OBJETO_ROBADO = "0";
	
	ObjetoRobado objetoRobado;
	Tipo tipoNombre,tipoCiudad,tipoCantidadPaises;
	Caracteristica caracteristica1, caracteristica2, caracteristica3; 
	 
	
	
	
	@Before
	public void setUp() {
		
		tipoNombre = new TipoNombre();
		tipoCiudad = new TipoCiudad();
		tipoCantidadPaises = new TipoCantidadPaises();
		
		caracteristica1 = new Caracteristica(tipoNombre, "Lampara de Aladino",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		caracteristica2 = new Caracteristica(tipoCiudad, "Cairo",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		caracteristica3 = new Caracteristica(tipoCantidadPaises, "4",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		
		objetoRobado= new ObjetoRobado();
		
	}
	
	
	@Test
	public void testPruebaQueUnObjetoRobadoPuedeAgregarCaracteristicas() {
		
		assertTrue(objetoRobado.agregarCaracteristica(caracteristica1));
		assertTrue(objetoRobado.agregarCaracteristica(caracteristica2));
		assertTrue(objetoRobado.agregarCaracteristica(caracteristica3));	
	}
	
	@Test
	public void testPruebaQueUnObjetoRobadoPuedeDevolverSuCiudadDeOrigen() throws CaracteristicaNoExistenteException {
		
		objetoRobado.agregarCaracteristica(caracteristica1);
		objetoRobado.agregarCaracteristica(caracteristica2);
		objetoRobado.agregarCaracteristica(caracteristica3);
		
		Tipo tipoCiudad2 = new TipoCiudad();

		assertEquals((objetoRobado.obtenerCaracteristica(tipoCiudad2)).getDescripcion(),"Cairo" );
		
	}
	
	@Test
	public void testPruebaQueUnObjetoRobadoDevuelveCantidadDeViajesQueTienePermitidoHacer() throws CaracteristicaNoExistenteException {
		
		objetoRobado.agregarCaracteristica(caracteristica1);
		objetoRobado.agregarCaracteristica(caracteristica2);
		objetoRobado.agregarCaracteristica(caracteristica3);
		
		assertEquals((objetoRobado.obtenerCaracteristica(tipoCantidadPaises)).getDescripcion(),"4");
		
	}

	@Test
	public void testPruebaQueUnObjetoRobadoDevuelveSuCantidadDePaisesQuePuedeRecorrer() throws NumberFormatException, CaracteristicaNoExistenteException {
		
		objetoRobado.agregarCaracteristica(caracteristica3);
		
		assertTrue(objetoRobado.obtenerCantidadDePaises()==4);
		
	}	
	
	@Test
	public void testUnObjetoRobadoDevuelveCorrectamenteLaDescripcionDeSuValor() throws CaracteristicaNoExistenteException {
		
		objetoRobado.agregarCaracteristica(caracteristica3);
		assertEquals(objetoRobado.obtenerDescripcionDelValor(), "Comun");
		
		objetoRobado = new ObjetoRobado();
		objetoRobado.agregarCaracteristica(new Caracteristica(tipoCantidadPaises, "5", DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		assertEquals(objetoRobado.obtenerDescripcionDelValor(), "Valioso");
		
		objetoRobado = new ObjetoRobado();
		objetoRobado.agregarCaracteristica(new Caracteristica(tipoCantidadPaises, "7", DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		assertEquals(objetoRobado.obtenerDescripcionDelValor(), "Muy Valioso");
	}
	
	@Test(expected = CaracteristicaNoExistenteException.class)
	public void testIntentarObtenerUnaCaracteristicaQueNoExisteEnUnObjetoRobadoDeberiaLanzarExcepcion() throws CaracteristicaNoExistenteException {
		
		objetoRobado.obtenerCaracteristica(tipoCiudad);
	}
}
