package unitarias;


import static org.junit.Assert.*;
import model.Jugador;
import model.Tiempo;

import org.junit.Before;
import org.junit.Test;

public class JugadorTests {

	private static final float MAX_TIEMPO = 144;
	private Jugador jugador;
	private int cantidad_horas;
	private Tiempo unTiempo = new Tiempo(MAX_TIEMPO);
	
	@Before
	public void setUp() {
		jugador = new Jugador(unTiempo);
		cantidad_horas = 154;
	}	

	@Test
	public void acuchillar(){
		jugador.acuchillar();
		int tiempo = (int) MAX_TIEMPO;
		tiempo--;
		assertEquals("El jugador tiene el tiempo bien cargado", jugador.tiempoRestante(), tiempo, .1);

		jugador.acuchillar();
		tiempo -= 2;
		assertEquals("El jugador tiene el tiempo bien cargado", jugador.tiempoRestante(), tiempo , .1);
		
		tiempo -= 3;
		jugador.acuchillar();
		assertEquals("El jugador tiene el tiempo bien cargado", jugador.tiempoRestante(), tiempo, .1);
		
		tiempo -= 3;
		jugador.acuchillar();
		assertEquals("El jugador tiene el tiempo bien cargado", jugador.tiempoRestante(), tiempo, .1);
	} 
}

