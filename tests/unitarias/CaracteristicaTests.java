package unitarias;

import static org.junit.Assert.*;
import model.Caracteristica;
import model.Tipo.*;
import model.GradoDificultad.*;

import org.junit.Before;
import org.junit.Test;



public class CaracteristicaTests {
	
	private static final String DIFICULTAD_LADRON = "0";
	private static final String DIFICULTAD_NOVATO = "1";
	private Tipo tipoPelo = new TipoPelo();
	private Tipo tipoAnimales = new TipoAnimales();
	
	@Test
	public void testPruebaQueCaracteristicaDevuelvaSusAtributosCorrectamenteUtilizandoLosMetodosCorrespondienteACadaAtributo() {
		
		Caracteristica caracteristica = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		assertEquals(caracteristica.getDescripcion(), "Rubio");
		assertEquals(caracteristica.getTipo(), tipoPelo);
		assertTrue(caracteristica.getDificultad().equals(DificultadGrado.darDificultad("0")));
	}

	@Test
	public void testPruebaQueCompararDosCaracteristicasConMismosAtributosDevuelveTrue() {
		
		Caracteristica caracteristica1 = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		Caracteristica caracteristica2 = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		assertTrue(caracteristica1.equals(caracteristica2) );

	}
	
	@Test
	public void testNegativoPruebaQueCompararDosCaracteristicasDeTipoDiferentesDaComoResultadoFalse() {
		
		Caracteristica caracteristica1 = new Caracteristica(tipoPelo, "rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		Caracteristica caracteristica2 = new Caracteristica(tipoAnimales, "rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		assertFalse(caracteristica1.equals(caracteristica2));

	}
	
	@Test
	public void testNegativoPruebaCompararDosCaracteristicasDeDificultadDiferentesDaComoResultadoFalse() {
		
		Caracteristica caracteristica1 = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		Caracteristica caracteristica2 = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		assertFalse(caracteristica1.equals(caracteristica2) );

	}
	
	@Test
	public void testNegativoPruebaCompararDosCaracteristicasDeDescripcionesDiferentesDaComoResultadoFalse() {
		
		Caracteristica caracteristica1 = new Caracteristica(tipoPelo, "Marron",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		Caracteristica caracteristica2 = new Caracteristica(tipoPelo, "Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		assertFalse(caracteristica1.equals(caracteristica2) );

	}
	
	@Test
	public void testPruebaQueCaracteristicaSabeMostrarseComoPista() {
		
		Caracteristica caracteristica1 = new Caracteristica(tipoPelo, "Marron",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		assertEquals(caracteristica1.mostrarComoPista(),"Me parecio haberle visto el pelo Marron" );
	}	
}
