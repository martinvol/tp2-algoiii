package unitarias;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Caracteristica;
import model.Ladron;
import model.Pais;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;

import java.util.ArrayList;


public class LadronTests {

	private static final String DIFICULTAD_LADRON = "0";
	
	private Caracteristica caract1,caract2,caract3,caract4,caract5,caract6;
	private Ladron ladron,ladron2;
	private Pais unPais, otroPais;
	
	private Tipo tipoPelo, tipoNombre, tipoSexo, tipoPasaTiempo, tipoVehiculo, tipoCaracteristica;
	@Before
	public void setUp() {
	
		tipoPelo= new TipoPelo();
		tipoNombre = new TipoNombre();
		tipoSexo = new TipoSexo();
		tipoPasaTiempo = new TipoPasaTiempo();
		tipoVehiculo = new TipoVehiculo();
		tipoCaracteristica = new TipoCaracteristica();
		
		
		caract1=new Caracteristica(tipoPelo,"Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract2=new Caracteristica(tipoNombre,"Martin",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract3=new Caracteristica(tipoSexo,"Masculino",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract4=new Caracteristica(tipoPasaTiempo,"Lol",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract5=new Caracteristica(tipoVehiculo,"207",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract6=new Caracteristica(tipoCaracteristica,"TatuajePitufo",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		ladron = new Ladron();
		ladron2 = new Ladron();
	}
	
	@Test
	public void testNegativoPruebaQueLadronSinPaisesAsignadosAlRecorridoNoPuedeViajar() {
		// Pruebo que no pueda viajar el ladron si no se le agrego paises.
		assertFalse(ladron.viajar());
	}
	
	@Test
	public void testPruebaQueElLadronDevuelvaNullCuandoSeLePidaElPaisActualSinHaberleAsignadoUnPaisAnteriormente() {
		// Pruebo que el pais actual sea null, porque no se le asigno ninguno.
		assertTrue(ladron.obtenerPaisActual() == null);
	}
	
	@Test
	public void testPruebaQueElLadronDevuelvaUnaListaVaciaAlPedirleSusCaracteristicasSinHaberleAgregadoAlgunaAntes() {
		
		ArrayList<Caracteristica> caracteristicas=ladron.obtenerCaracteristicas();
		assertTrue(caracteristicas.isEmpty());
	}
	@Test
	public void testPruebaQueLadronPuedeAgregarCaracteristicasCorrectamente() {
		// Pruebo que el pais actual sea null, porque no se le asigno ninguno.
		assertTrue(ladron.agregarCaracteristica(caract1));
		assertTrue(ladron.agregarCaracteristica(caract2));
		assertTrue(ladron.agregarCaracteristica(caract3));
		assertFalse(ladron.agregarCaracteristica(caract1));
		assertFalse(ladron.agregarCaracteristica(caract2));
		assertFalse(ladron.agregarCaracteristica(caract3));
	}
	
	@Test
	public void testPruebaQueLadronDevuelveSusCaracteristicasAsignadasAnteriormenteEnUnaLista() throws CaracteristicaNoExistenteException {
		// Pruebo ver que devuelva la caracteristica acorde al tipo que se le agrego.
		ladron.agregarCaracteristica(caract1);
		Caracteristica caracteristica=ladron.obtenerCaracteristicaDeTipo(tipoPelo);
		assertTrue( caracteristica.getDescripcion() == "Rubio" );
	}
	
	
	@Test
	public void testPruebaQueLadronDevuelveCeroSiSeLePidenLosPaisesRestantesY_A_EsteNoSeLeAsignaronPaises() {
		
		assertTrue(ladron.paisesRestantes()==0);
		
	}

	
	@Test
	public void testPruebaQueLadronDevuelveSusCaracteristicasAsignadasAnteriormenteEnUnaLista2() throws CaracteristicaNoExistenteException {
		// Pruebo ver que devuelva la caracteristica acorde al tipo que se le agrego. 2 
		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		ladron.agregarCaracteristica(caract3);
		ladron.agregarCaracteristica(caract4);
		ladron.agregarCaracteristica(caract5);
		Caracteristica caracteristica=ladron.obtenerCaracteristicaDeTipo(tipoSexo);
		assertTrue( caracteristica.getDescripcion() == "Masculino" );
	}

	@Test
	public void testPruebaQueLadronDevuelveTrueAlUsarElMetodoCoincidenciaConCaracteristicasSiSeLePasaCaracteristicasAnteriormenteAgregadas() {
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>(); 
		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		ladron.agregarCaracteristica(caract3);
		ladron.agregarCaracteristica(caract4);
		ladron.agregarCaracteristica(caract5);
		
		caracteristicas.add(caract1);
		caracteristicas.add(caract5);
	
		assertTrue(ladron.coincidenciaConCaracteristicas(caracteristicas));
		
		caracteristicas.add(caract6);
		assertFalse(ladron.coincidenciaConCaracteristicas(caracteristicas));
		
		ladron.agregarCaracteristica(caract6);
		assertTrue(ladron.coincidenciaConCaracteristicas(caracteristicas));
		
	}

	@Test
	public void testNegativoPruebaQueLadronDevuelveFalseAlUsarElMetodoCoincidenciaConCaracteristicasSiSeLePasaCaracteristicasQueNoFueronAgregadas() {
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>(); 
		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		ladron.agregarCaracteristica(caract3);
		ladron.agregarCaracteristica(caract4);
		ladron.agregarCaracteristica(caract5);
		
		caracteristicas.add(caract1);
		caracteristicas.add(caract6);
	
		assertFalse(ladron.coincidenciaConCaracteristicas(caracteristicas));
	}
	
	@Test
	public void testPruebaQueSePuedeAgregarPaisesAlRecorridoCorrectamente() {
		// Pruebo agregar un pais al recorrido del ladron.

		unPais = new Pais();
		
		assertTrue(ladron.agregarRecorrido(unPais));	
		
	}
	@Test
	public void testPruebaQueSePuedeAgregarMasDeUnPaisAlRecorridoCorrectamente() {
		// Pruebo agregar otro pais al recorrido del ladron.

		
		unPais = new Pais();
		ladron.agregarRecorrido(unPais);
		
		otroPais = new Pais();
		
		assertTrue(ladron.agregarRecorrido(otroPais));	
		
	}
	
	@Test
	public void testPruebaQueElLadronDevuelveElPaisActualCorrectamente() {
		// Pruebo que me devuelva el pais actual que deberia estar por el orden de agregado.

		unPais = new Pais();
		ladron.agregarRecorrido(unPais);
		
		assertTrue(ladron.obtenerPaisActual() == unPais);
	}
	
	@Test
	public void testPruebaQueElLadronPuedaViajarCorrectamenteUnaVezAsignadosPaisesAlRecorrido() {
		// Pruebo que el ladron pueda viajar ahora q se le agregaron paises.

		
		unPais = new Pais();
		ladron.agregarRecorrido(unPais);
		
		otroPais = new Pais();
		
		ladron.agregarRecorrido(otroPais);	
		
		assertTrue(ladron.viajar());
	}
	@Test
	public void testPruebaQueElPaisActualCambiaAlViajarCorrectamente() {
		// Pruebo que el ladron se haya movido de pais al usar el metodo viajar.

		
		unPais = new Pais();
		ladron.agregarRecorrido(unPais);
		
		otroPais = new Pais();
		
		ladron.agregarRecorrido(otroPais);	
		
		ladron.viajar();
		
		assertTrue(ladron.obtenerPaisActual() == otroPais);
		assertFalse(ladron.viajar());
		
	}
	
	@Test
	public void testPruebaQueLadronAlLlegarAlUltimoPaisDelRecorridoNoPuedeIrseAOtroPais() {
		// Pruebo que el ladron una vez que tiene paises, se quede en el ultimo de la cola.

		
		unPais = new Pais();
		ladron.agregarRecorrido(unPais);
		
		otroPais = new Pais();
		
		ladron.agregarRecorrido(otroPais);	
		
		ladron.viajar();
		ladron.viajar();
		ladron.viajar();		
		
		assertTrue(ladron.obtenerPaisActual() == otroPais);
		
	}
	
	@Test
	public void testPruebaQueLadronDevuelveLaCantidadDePaisesDeSuRecorridoAsignado() {
		
		unPais = new Pais();
		otroPais = new Pais();
		ladron.agregarRecorrido(unPais);		
		ladron.agregarRecorrido(otroPais);	
		
		assertTrue(ladron.paisesRestantes()==1);
		
	}
	
	@Test
	public void testPruebaQueUnLadronSeaIgualA_OtroSiTieneLasMismasCaracteristicas() {
		assertTrue(ladron.equals(ladron2));
		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		
		ladron2.agregarCaracteristica(caract1);
		ladron2.agregarCaracteristica(caract2);
		
		assertTrue(ladron.equals(ladron2));
		ladron2.agregarCaracteristica(caract3);
		assertFalse(ladron.equals(ladron2));
		
		ladron.agregarCaracteristica(caract3);
		assertTrue(ladron.equals(ladron2));
	}

	@Test
	public void testPruebaQueUnLadronDevuelvaPistasPeroSinDarSuNombrePorMasQueLoTengaEnLasCaracteristicas() {

		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		ladron.agregarCaracteristica(caract3);
		ladron.agregarCaracteristica(caract4);
		ladron.agregarCaracteristica(caract5);
		ladron.agregarCaracteristica(caract6);

		ArrayList<Caracteristica> pistas= ladron.obtenerCaracteristicasParaPistas();
		
		assertFalse(pistas.contains(caract2));
	}
	
	@Test(expected = CaracteristicaNoExistenteException.class)
	public void testPedirleUnaCaracteristicaAUnLadronQueNoPoseeDeberiaLanzarUnaExcepcion() throws CaracteristicaNoExistenteException {
		ladron.obtenerCaracteristicaDeTipo(new TipoNombre());
	}
}
