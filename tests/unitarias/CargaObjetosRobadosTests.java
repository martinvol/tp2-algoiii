package unitarias;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import model.BancoObjetosRobados;
import model.ObjetoRobado;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import parser.Parser;

public class CargaObjetosRobadosTests {
	
	BancoObjetosRobados objetosRobados;
	Parser parser;
	
	
	@Before
	public void setup() throws ParserConfigurationException, TransformerException, SAXException, IOException{
		
		this.parser = new Parser();
		this.objetosRobados = parser.crearBancoObjetosRobados("resources/datos/objetos.xml");
	}
	
	@Test
	public void testPruebaQueCrearBancoObjetosRobadosNoEsNull() {
		assertNotNull(this.objetosRobados);
	}
	
	@Test
	public void testPruebaQueObtenerUnObjetoRandomNoEsNull(){
		
		ObjetoRobado objeto = this.objetosRobados.darUnObjetoRobadoRandom();
		assertNotNull(objeto);
	}
	

}
