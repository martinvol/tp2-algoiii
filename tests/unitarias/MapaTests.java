package unitarias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Caracteristica;
import model.Mapa;
import model.Pais;
import model.Excepciones.PaisNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.Lugares.Lugar;
import model.Tipo.*;

import org.junit.Before;
import org.junit.Test;

public class MapaTests {

	private static final String DIFICULTAD_UNO = "1";
	private static final String DIFICULTAD_DOS = "2";

	
	private Pais pais,pais2,pais3,pais4;
	private Caracteristica caract1,caract2,caract3,caract4,caract5, pista1,pista2,pista3;
	private Tipo tipoCiudad, tipoBandera, tipoMoneda, tipoCiudad1,tipoAnimales;
	private Mapa mapa;
	
	
	@Before
	public void setUp() {
		
		mapa= new Mapa();
		
		pais = new Pais();
		pais2 = new Pais();
		pais3 = new Pais();
		pais4 = new Pais();
		
		tipoAnimales = new TipoAnimales();
		tipoCiudad1 = new TipoCiudad();
		tipoCiudad = new TipoCiudad();
		tipoMoneda = new TipoMoneda();
		tipoBandera = new TipoBandera();
		
		caract1=new Caracteristica(tipoCiudad,"Buenos Aires",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract2=new Caracteristica(tipoBandera,"Azul, Blanca y Azul",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract3=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_DOS));
		
		caract4=new Caracteristica(tipoMoneda,"Dolar",DificultadGrado.darDificultad(DIFICULTAD_DOS));
		
		caract5=new Caracteristica(tipoCiudad1,"Buenos Aires",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		
		pista1=new Caracteristica(tipoAnimales,"Zorro",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		pista2=new Caracteristica(tipoBandera,"Azul, Blanca y Azul",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		pista3=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_DOS));

		
		pais.agregarCaracteristica(caract1);
		pais2.agregarCaracteristica(caract2);
		pais3.agregarCaracteristica(caract3);

	}
	
	@Test
	public void testPruebaQueElMapaPuedeGuardarPaisesCorrectamente() {
		
		assertTrue(mapa.agregarPais(pais));
		assertTrue(mapa.agregarPais(pais2));
		assertTrue(mapa.agregarPais(pais3));
		assertTrue(mapa.agregarPais(pais4));
		assertFalse(mapa.agregarPais(pais4));
	}
	
	@Test
	public void testPruebaQueElMapaDevuelveElPaisConElNombreDeLaCiudadQueSePidio() {
		mapa.agregarPais(pais);
		mapa.agregarPais(pais2);
		mapa.agregarPais(pais3);
		mapa.agregarPais(pais4);
		
		assertEquals(mapa.obtenerPaisPorCiudad(caract1),pais);
		assertEquals(mapa.obtenerPaisPorCiudad(caract2),pais2);
		assertEquals(mapa.obtenerPaisPorCiudad(caract3),pais3);
		assertFalse(mapa.obtenerPaisPorCiudad(caract3)== pais);
		assertFalse(mapa.obtenerPaisPorCiudad(caract2)== pais);
		assertFalse(mapa.obtenerPaisPorCiudad(caract1)== pais2);
		
		assertEquals(mapa.obtenerPaisPorCiudad(caract5),pais);
	}
	
	@Test
	public void testPruebaQueElMapaBorreLasPistasDeLosLugares() {
		
		ArrayList<Lugar> lugares= pais.obtenerLugares();
		
		lugares.get(0).agregarCaracteristica(pista3);
		lugares.get(1).agregarCaracteristica(pista1);
		lugares.get(2).agregarCaracteristica(pista2);
		
		ArrayList<Caracteristica> pistas= lugares.get(0).darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(),"Cambio todo su dinero a Pesos");
		
		mapa.agregarPais(pais);
		assertTrue(mapa.limpiarPaises());
		
		pistas= lugares.get(0).darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(),"Nunca vi a nadie con esa descripcion");	
	}
	
	@Test
	public void testPruebaQueElMapaBorreLasPistasDeLosLugaresSinAgregarPaisesAlMapa() {
		assertTrue(mapa.limpiarPaises());
		
	}
	
	@Test(expected = PaisNoExistenteException.class)
	public void testPedirUnPaisPorCiudadAlMapaQueNoExisteDebeLanzarUnaExcepcion() {
		mapa.obtenerPaisPorCiudad(caract1);
	}
}
