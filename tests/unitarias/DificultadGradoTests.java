package unitarias;

import static org.junit.Assert.*;

import org.junit.Test;

import model.GradoDificultad.*;

public class DificultadGradoTests {

	DificultadGrado dif1,dif2,dif3,dif4,dif5,dif6,dif7,dif8;
		
	@Test
	public void testPruebaQueSeDosDificultadesSepanCompararse() {
			
		dif1= new DificultadGradoUno();
		dif2=new DificultadGradoUno();
		assertTrue(dif1.equals(dif2));
			
		dif3=new DificultadGradoDos();
		dif4=new DificultadGradoDos();
		assertTrue(dif3.equals(dif4));
		
		dif5=new DificultadGradoTres();
		dif6=new DificultadGradoTres();
		assertTrue(dif5.equals(dif6));
			
		dif7=new DificultadGradoCuatro();
		dif8=new DificultadGradoCuatro();
		assertTrue(dif7.equals(dif8));
	}

				
}
