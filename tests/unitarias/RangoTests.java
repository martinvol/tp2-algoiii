package unitarias;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Rango;

import model.GradoDificultad.DificultadGrado;

public class RangoTests {

	private static final int CANT_INIT_ARRESTOS = 0;
	private static final int CANT_INVESTIGADOR_ARRESTOS = 5;
	private static final int CANT_DETECTIVE_ARRESTOS = 10;
	private static final int CANT_SARGENTO_ARRESTOS = 20;

	Rango rango;
	
	@Before
	public void setUp() {
		rango= new Rango();
	}
	
	@Test
	public void testPruebaQueUnRangoVacioDeberiaTenerCantidadDeArrestosCero() {
		assertTrue(rango.getCantidadDeArrestos() == CANT_INIT_ARRESTOS);
	}
	
	@Test
	public void testPruebaQueLaDescripcionDeUnRangoSinArrestosDeberiaSerNovato() {
		
		assertTrue(rango.darGradoDeDificultad().equals(DificultadGrado.darDificultad("1")));
	
	}
	@Test
	public void testPruebaQueElRangoPuedeAgregarUnArresto() {
		rango.agregarArresto();
		assertTrue(rango.getCantidadDeArrestos() == 1);
	}
	
	@Test
	public void testPruebaQueElRangoPuedeAgregarCincoArrestos() {
		
		for (int i = 1; i <= CANT_INVESTIGADOR_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.getCantidadDeArrestos() == CANT_INVESTIGADOR_ARRESTOS);
	}
	@Test
	public void testPruebaQueLaDescripcionDeUnRangoConCincoArrestosDeberiaSerInvestigador() {
		
		for (int i = 1; i <= CANT_INVESTIGADOR_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.darGradoDeDificultad().equals(DificultadGrado.darDificultad("2")));
	}
	@Test
	public void testPruebaQueElRangoPuedeAgregarDiezArrestos() {
		
		for(int i = 1; i <= CANT_DETECTIVE_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.getCantidadDeArrestos() == CANT_DETECTIVE_ARRESTOS);
	}
	@Test
	public void testPruebaQueLaDescripcionDeUnRangoConDiezArrestosDeberiaSerDetective() {
		
		for(int i = 1; i <= CANT_DETECTIVE_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.darGradoDeDificultad().equals(DificultadGrado.darDificultad("3")));
	}
	@Test
	public void testPruebaQueElRangoPuedeAgregarVeinteArrestos() {
		
		for(int i = 1; i <= CANT_SARGENTO_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.getCantidadDeArrestos() == CANT_SARGENTO_ARRESTOS);
	}
	@Test
	public void testPruebaQueLaDescripcionDeUnRangoConVeinteArrestosDeberiaSerSargento() {
		
		for(int i = 1; i <= CANT_SARGENTO_ARRESTOS; i++) rango.agregarArresto();
		
		assertTrue(rango.darGradoDeDificultad().equals(DificultadGrado.darDificultad("4")));
	}
}
