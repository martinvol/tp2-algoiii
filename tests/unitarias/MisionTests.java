package unitarias;



import static org.junit.Assert.*;
import model.BancoObjetosRobados;
import model.Caracteristica;
import model.Expediente;
import model.Ladron;
import model.Limitrofe;
import model.Mapa;
import model.Mision;
import model.ObjetoRobado;
import model.Pais;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.Tipo;
import model.Tipo.TipoCantidadPaises;
import model.Tipo.TipoCiudad;

import org.junit.Before;
import org.junit.Test;

public class MisionTests {


	private static final String DIFICULTAD_OBJETO_ROBADO = "0";
	private Expediente expediente;
	private Mapa mapa;
	private BancoObjetosRobados objetosRobados;
	private Mision mision;
	private ObjetoRobado objetoRobado1;
	private Ladron ladron1;
	private Tipo tipoCiudad,tipoCantidadPaises;
	private Pais pais,pais2,pais3,pais4,pais5,pais6,pais7,pais8,pais9,pais10,pais11,pais12,pais13,pais14,pais15;
	Caracteristica caract1, caract2, caract3;
	private Limitrofe limitrofe1,limitrofe2,limitrofe3,limitrofe4,limitrofe5,limitrofe6,limitrofe7,limitrofe8,limitrofe9,limitrofe10,limitrofe11,limitrofe12,
	limitrofe13,limitrofe14;
	
	@Before
	public void setUp() {
		
		expediente = new Expediente();
		mapa = new Mapa();
		objetosRobados = new BancoObjetosRobados();
		

		objetoRobado1 = new ObjetoRobado();
		ladron1=new Ladron();
		tipoCiudad= new TipoCiudad();
		tipoCantidadPaises = new TipoCantidadPaises();
		
		pais= new Pais();
		pais2= new Pais();
		pais3= new Pais();
		pais4= new Pais();
		pais5= new Pais();
		pais6= new Pais();
		pais7= new Pais();
		pais8= new Pais();
		pais9= new Pais();
		pais10= new Pais();
		pais11= new Pais();
		pais12= new Pais();
		pais13= new Pais();
		pais14= new Pais();
		pais15= new Pais();
	
		
		
		limitrofe1= new Limitrofe(pais,pais2,20);
		limitrofe2= new Limitrofe(pais,pais3,20);
		limitrofe3= new Limitrofe(pais2,pais4,20);
		limitrofe4= new Limitrofe(pais2,pais5,20);
		limitrofe5= new Limitrofe(pais3,pais6,20);
		limitrofe6= new Limitrofe(pais3,pais7,20);
		limitrofe7= new Limitrofe(pais4,pais8,20);
		limitrofe8= new Limitrofe(pais4,pais9,20);
		limitrofe9= new Limitrofe(pais5,pais10,20);
		limitrofe10= new Limitrofe(pais5,pais11,20);
		limitrofe11= new Limitrofe(pais6,pais12,20);
		limitrofe12= new Limitrofe(pais6,pais13,20);
		limitrofe13= new Limitrofe(pais7,pais14,20);
		limitrofe14= new Limitrofe(pais7,pais15,20);

		pais.setLimitrofe(limitrofe1);
		pais.setLimitrofe(limitrofe2);

		pais2.setLimitrofe(limitrofe3);
		pais2.setLimitrofe(limitrofe4);
		
		pais3.setLimitrofe(limitrofe5);
		pais3.setLimitrofe(limitrofe6);
		
		pais4.setLimitrofe(limitrofe7);
		pais4.setLimitrofe(limitrofe8);
		
		pais5.setLimitrofe(limitrofe9);
		pais5.setLimitrofe(limitrofe10);
		
		pais6.setLimitrofe(limitrofe11);
		pais6.setLimitrofe(limitrofe12);
		
		pais7.setLimitrofe(limitrofe13);
		pais7.setLimitrofe(limitrofe14);
		
		caract1=new Caracteristica(tipoCiudad, "Cairo",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		pais.agregarCaracteristica(caract1);
		
		caract2=new Caracteristica(tipoCiudad, "Cairo",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		caract3=new Caracteristica(tipoCantidadPaises, "4",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO));
		objetoRobado1.agregarCaracteristica(caract2);
		objetoRobado1.agregarCaracteristica(caract3);
		
		expediente.agregarLadron(ladron1);
		objetosRobados.agregarObjetoRobado(objetoRobado1);
		mapa.agregarPais(pais);
		
		mision = new Mision();

		
	}
	
	
	@Test
	public void testPruebaQueMisionCreaUnaNuevaMisionCorrectamente() throws CaracteristicaNoExistenteException {
		
		mision.agregarBancoObjetosRobados(objetosRobados);
		mision.agregarExpedientes(expediente);
		mision.agregarMapa(mapa);
		
		assertTrue(mision.nuevaMision());
	}
	
	@Test
	public void testPruebaQueMisionNoPuedeCrearUnaNuevaMisionSiNoSeLeAsignaElMapaElExpedienteElBancoDeObjetosRobados() throws CaracteristicaNoExistenteException {
				
		assertFalse(mision.nuevaMision());
		
		mision.agregarBancoObjetosRobados(objetosRobados);
		assertFalse(mision.nuevaMision());
		
		mision.agregarExpedientes(expediente);
		assertFalse(mision.nuevaMision());
		
		mision.agregarMapa(mapa);		
		assertTrue(mision.nuevaMision());
	}
	
	@Test
	public void testPruebaQueMisionDevuelveNullAlPedirleUnLadronO_UnPaisO_UnObjetoRobadoSiNoSeLeAsignoUnExpediente() throws CaracteristicaNoExistenteException {
		
		assertEquals(mision.darLadron(),null);
		assertEquals(mision.darObjetoRobado(),null);
		assertEquals(mision.darPaisDeMision(),null);
		
		assertFalse(mision.nuevaMision());
		
		assertEquals(mision.darLadron(),null);
		assertEquals(mision.darObjetoRobado(),null);
		assertEquals(mision.darPaisDeMision(),null);
		
		mision.agregarBancoObjetosRobados(objetosRobados);
		
		
		assertFalse(mision.nuevaMision());
		
		assertEquals(mision.darLadron(),null);
		assertEquals(mision.darObjetoRobado(),null);
		assertEquals(mision.darPaisDeMision(),null);
		
		mision.agregarExpedientes(expediente);
		
		assertFalse(mision.nuevaMision());
		
		assertEquals(mision.darLadron(),null);
		assertEquals(mision.darObjetoRobado(),null);
		assertEquals(mision.darPaisDeMision(),null);
	}
	
	
	
	@Test
	public void testPruebaQueMisionDevuelvaUnLadronCorrectamenteLuegoDePedirleUnaNuevaMision() throws CaracteristicaNoExistenteException {
		mision.agregarBancoObjetosRobados(objetosRobados);
		mision.agregarExpedientes(expediente);
		mision.agregarMapa(mapa);
		
		mision.nuevaMision();
		
		assertEquals(mision.darLadron(),ladron1);
		
	}
	@Test
	public void testPruebaQueDevuelvaUnObjetoRobadoCorrectamenteLuegoPedirUnaNuevaMision() throws CaracteristicaNoExistenteException {
		mision.agregarBancoObjetosRobados(objetosRobados);
		mision.agregarExpedientes(expediente);
		mision.agregarMapa(mapa);
		
		mision.nuevaMision();
		assertEquals(mision.darObjetoRobado(),objetoRobado1);
	}
	@Test
	public void testPruebaQueDevuelvaElPaisDondeEmpiezaLaMision() throws CaracteristicaNoExistenteException {
		mision.agregarBancoObjetosRobados(objetosRobados);
		mision.agregarExpedientes(expediente);
		mision.agregarMapa(mapa);
		
		mision.nuevaMision();
		assertEquals(mision.darPaisDeMision(),pais);
	}
	
	@Test
	public void testPruebaQueMisionDevuelveUnLadronConUnRecorridoEnBaseAlObjetoRobado() throws CaracteristicaNoExistenteException {
		mision.agregarBancoObjetosRobados(objetosRobados);
		mision.agregarExpedientes(expediente);
		mision.agregarMapa(mapa);
		
		mision.nuevaMision();
		Ladron ladron2=mision.darLadron();
		assertTrue(ladron2.paisesRestantes()==3);
		
		assertEquals(ladron2.obtenerPaisActual(),pais);
		
		assertTrue(ladron2.viajar());
		assertTrue(ladron2.viajar());
		assertTrue(ladron2.viajar());
		assertFalse(ladron2.viajar());
	}
	
	
}
