package unitarias;

import static org.junit.Assert.*;
import model.Caracteristica;
import model.ObjetoRobado;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.Excepciones.ContenedorVacioException;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;
import model.BancoObjetosRobados;

import org.junit.Before;
import org.junit.Test;

public class BancoDeObjetosRobadosTests {
	
	private static final String DIFICULTAD_OBJETO_ROBADO = "0";
	private ObjetoRobado objetoRobado1;
	private ObjetoRobado objetoRobado2;
	private ObjetoRobado objetoRobado3;	
	private Tipo tipoNombre,tipoCiudad,tipoCantidadPaises;
	private BancoObjetosRobados banco;
	
	@Before
	public void SetUp() {
		
		tipoNombre = new TipoNombre();
		tipoCiudad = new TipoCiudad();
		tipoCantidadPaises = new TipoCantidadPaises();
		
		
		objetoRobado1= new ObjetoRobado();
		objetoRobado1.agregarCaracteristica(new Caracteristica(tipoNombre, "Lampara de Aladino",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado1.agregarCaracteristica(new Caracteristica(tipoCiudad, "Cairo",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado1.agregarCaracteristica(new Caracteristica(tipoCantidadPaises, "4",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		
		objetoRobado2= new ObjetoRobado();
		objetoRobado2.agregarCaracteristica(new Caracteristica(tipoNombre, "Guitarra",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado2.agregarCaracteristica(new Caracteristica(tipoCiudad, "New York",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado2.agregarCaracteristica(new Caracteristica(tipoCantidadPaises, "4",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		
		objetoRobado3= new ObjetoRobado();
		objetoRobado3.agregarCaracteristica(new Caracteristica(tipoNombre, "Computadora",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado3.agregarCaracteristica(new Caracteristica(tipoCiudad, "Buenos Aires",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		objetoRobado3.agregarCaracteristica(new Caracteristica(tipoCantidadPaises, "4",DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
		
		banco = new BancoObjetosRobados();
	}
	

	@Test
	public void testAgregarUnObjetoRobadoTendriaQueDevolverTrue() {
		
		assertTrue(banco.agregarObjetoRobado(objetoRobado1));
	}
	
	@Test
	public void testPedirObjetoRobadoLuegoDeAgregarUnObjetoRobadoTieneQueDevolverElObjetoRobadoAgregado() {
		
		banco.agregarObjetoRobado(objetoRobado1);
		assertEquals(banco.darUnObjetoRobadoRandom(),objetoRobado1);
	}
	
	@Test(expected = ContenedorVacioException.class)
	public void testPedirUnObjetoRobadoRandomAUnBancoVacioDeberiaLanzarExcepcion() {
		
		BancoObjetosRobados unBanco = new BancoObjetosRobados();
		unBanco.darUnObjetoRobadoRandom();
	}
	
	@Test(expected = CaracteristicaNoExistenteException.class)
	public void testPedirleAlBancoUnObjetoCuyoNombreNoEsCorrectoDebeLanzarUnaExcepcion() throws CaracteristicaNoExistenteException {
		
		banco.darUnObjetoRobadoConNombre(new Caracteristica(new TipoNombre(), "Alfombra voladora", DificultadGrado.darDificultad(DIFICULTAD_OBJETO_ROBADO)));
	}
}
