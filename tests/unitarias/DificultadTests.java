package unitarias;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Caracteristica;
import model.Dificultad;
import model.Ladron;
import model.Rango;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;
import model.Pais;

public class DificultadTests {
	
	private static final String DIFICULTAD_LADRON = "0";
	private static final String DIFICULTAD_NOVATO = "1";
	private static final String DIFICULTAD_INVESTIGADOR = "2";
	
	private Ladron ladron,ladron2;
	private Rango rango;
	private Dificultad dificultad;
	private Caracteristica caract1,caract2,caract3,caract4,caract5,caract6,caract7,caract8,caract9,caract10,caract11,caract12,caract13;
	private Pais pais1,pais2,pais3,pais4;

	
	private Tipo tipoCiudad, tipoMoneda, tipoLider,tipoPelo, tipoNombre, tipoSexo, tipoPasaTiempo, tipoVehiculo, tipoCaracteristica, tipoBandera;
	
	@Before
	public void setUp() {
		
		ladron = new Ladron();
		ladron2 = new Ladron();
		rango= new Rango();

		tipoCiudad= new TipoCiudad();
		tipoMoneda = new TipoMoneda();
		tipoLider = new TipoLider();
		tipoBandera = new TipoBandera();
		
		tipoPelo = new TipoPelo();
		tipoNombre = new TipoNombre();
		tipoSexo = new TipoSexo();
		tipoPasaTiempo = new TipoPasaTiempo();
		tipoVehiculo = new TipoVehiculo();
		tipoCaracteristica = new TipoCaracteristica();
		
		pais1=new Pais();
		pais2=new Pais();
		pais3=new Pais();
		pais4=new Pais();
		
		caract7=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		caract8=new Caracteristica(tipoCiudad,"Buenos Aires",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		caract9=new Caracteristica(new TipoAnimales(),"Zorros",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		caract13=new Caracteristica(tipoBandera,"Azul, blanca y Azul",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		
		caract10=new Caracteristica(tipoMoneda,"Dolar",DificultadGrado.darDificultad(DIFICULTAD_INVESTIGADOR));
		caract11=new Caracteristica(tipoCiudad,"New York",DificultadGrado.darDificultad(DIFICULTAD_NOVATO));
		caract12=new Caracteristica(tipoLider,"Manu",DificultadGrado.darDificultad(DIFICULTAD_INVESTIGADOR));
		
		pais1.agregarCaracteristica(caract7);
		pais1.agregarCaracteristica(caract8);
		pais1.agregarCaracteristica(caract9);
		pais1.agregarCaracteristica(caract13);
		
		pais2.agregarCaracteristica(caract10);
		pais2.agregarCaracteristica(caract11);
		pais2.agregarCaracteristica(caract12);
		
		caract1=new Caracteristica(tipoPelo,"Rubio",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract2=new Caracteristica(tipoNombre,"Martin",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract3=new Caracteristica(tipoSexo,"Masculino",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract4=new Caracteristica(tipoPasaTiempo,"Lol",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract5=new Caracteristica(tipoVehiculo ,"207",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract6=new Caracteristica(tipoCaracteristica,"TatuajePitufo",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		
		ladron.agregarCaracteristica(caract1);
		ladron.agregarCaracteristica(caract2);
		ladron.agregarCaracteristica(caract3);
		ladron.agregarCaracteristica(caract4);
		ladron.agregarCaracteristica(caract5);
		ladron.agregarCaracteristica(caract6);
		
		ladron.agregarRecorrido(pais1);
		ladron.agregarRecorrido(pais3);
		ladron.agregarRecorrido(pais4);
		
		dificultad = new Dificultad();
	}

	
	@Test
	public void testPruebaQueDificultadAgregueUnLadronCorrectamenteDevolviendoTrue() {
		
		assertTrue(dificultad.agregarLadron(ladron));
		assertTrue(dificultad.agregarLadron(ladron2));	
	}
	
	@Test
	public void testPruebaQuePasandoleUnRangoYUnPaisCargaEnElPaisLasPistasCorrespondientesAlRangoDelJugador() {
		
		dificultad.agregarLadron(ladron);
		assertTrue(dificultad.guardarPista(pais2,rango));	
	}
	
}
