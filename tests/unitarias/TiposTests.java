package unitarias;


import static org.junit.Assert.*;
import model.Tipo.*;

import org.junit.Before;
import org.junit.Test;

public class TiposTests {

	Tipo tipoPelo, tipoPelo2, tipoBandera, tipoAnimales, tipoSexo, tipoCaracteristica;

	@Test
	public void testPruebaMetodoQueDevuelveElTextoQueModelaLaPistaSeaElCorrecto() {

		tipoPelo=new TipoPelo();
		tipoBandera= new TipoBandera();
		tipoAnimales= new TipoAnimales();
		assertEquals(tipoPelo.darModeloPista(),"Me parecio haberle visto el pelo ");
		assertEquals(tipoBandera.darModeloPista(),"Me parecio haberlo subir a un avion que tenia una bandera de color ");
		assertEquals(tipoAnimales.darModeloPista(),"Demostro mucho interes por los ");
		assertFalse(tipoAnimales.darModeloPista().equals("Hola"));
		
	}
	
	@Test
	public void testPruebaQueAlCompararDosInstanciasDistintasDelMismoTipoDevuelvaTrue() {

		tipoPelo= new TipoPelo();
		tipoPelo2= new TipoPelo();
		tipoBandera = new TipoBandera();
		assertTrue(tipoPelo.equals(tipoPelo2));
		assertFalse(tipoPelo.equals(tipoBandera));

		
	}
}
