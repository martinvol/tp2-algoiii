package unitarias;

import static org.junit.Assert.*;
import model.Limitrofe;
import model.Pais;
import model.Excepciones.PaisNoExistenteException;

import org.junit.Test;

public class LimitrofeTests {

	
	private static final int DISTANCIA_TEST = 20;

	@Test
	public void testUnLimitrofeDebeDevolverCorrectamenteLaDistancia() {
		Pais origen = new Pais();
		Pais destino = new Pais();
		
		Limitrofe conexionOrigenDestino = new Limitrofe(origen, destino, DISTANCIA_TEST);
		assertEquals(conexionOrigenDestino.obtenerDistancia(), DISTANCIA_TEST, 0);
	}
	
	@Test
	public void testUnLimitrofeDebeDevolverCorrectamenteSusPaises() {
		
		Pais origen = new Pais();
		Pais destino = new Pais();
		Pais otroPais = new Pais();
		
		Limitrofe conexionOrigenDestino = new Limitrofe(origen, destino, DISTANCIA_TEST);
		assertEquals(conexionOrigenDestino.obtenerLimitrofeDesde(origen), destino);
		assertEquals(conexionOrigenDestino.obtenerLimitrofeDesde(destino), origen);
	}
	
	@Test(expected = PaisNoExistenteException.class)
	public void testIntentarObtenerDesdeUnPaisQueNoEstaEnLaRelacionDeLimitrofeDeberiaLanzarError() {
		

		Pais origen = new Pais();
		Pais destino = new Pais();
		Pais otroPais = new Pais();
		
		Limitrofe conexionOrigenDestino = new Limitrofe(origen, destino, DISTANCIA_TEST);
		
		conexionOrigenDestino.obtenerLimitrofeDesde(otroPais);
	}
}
