package unitarias;

import static org.junit.Assert.*;

import org.junit.Before;

import java.util.ArrayList;

import org.junit.Test;

import model.Caracteristica;
import model.Expediente;
import model.Ladron;
import model.Excepciones.ContenedorVacioException;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.*;

public class ExpedienteTests {

	private static final String DIFICULTAD_LADRON = "0";
	private Expediente expediente;
	private Ladron ladron1,ladron2;
	private Caracteristica caract1,caract2,caract3,caract4,caract5,caract6,caract7,caract8;
	
	private Tipo tipoPelo, tipoSexo, tipoPasaTiempo, tipoVehiculo;
	
	@Before
	public void SetUp() {
		
		tipoPelo= new TipoPelo();
		tipoSexo = new TipoSexo();
		tipoPasaTiempo = new TipoPasaTiempo();
		tipoVehiculo = new TipoVehiculo();
		expediente = new Expediente();
		ladron1 = new Ladron();
		ladron2 = new Ladron();
		caract1 = new Caracteristica(tipoPelo,"Marron",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract2 = new Caracteristica(tipoSexo,"Femenino",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract3 = new Caracteristica(tipoVehiculo,"Motocicleta",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract4 = new Caracteristica(tipoPasaTiempo,"Tennis",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract5 = new Caracteristica(tipoPelo,"Marron",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract6 = new Caracteristica(tipoSexo,"Masculino",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract7 = new Caracteristica(tipoVehiculo,"Motocicleta",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
		caract8 = new Caracteristica(tipoVehiculo,"Jet",DificultadGrado.darDificultad(DIFICULTAD_LADRON));
	}
	
	
	@Test
	public void testPruebaQueUnExpedienteIndicaCorrectamenteSiEstaVacio() {
	
		assertTrue(expediente.estaVacio());
		assertTrue(expediente.agregarLadron(ladron1));
		
		assertFalse(expediente.estaVacio());
	}
	
	@Test
	public void testPruebaQueUnExpedienteVacioDevuelveUnaListaVaciaAlPasarleCaracteristicasParaBuscarLadrones() {
				
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		caracteristicas.add(caract8);
		
		ArrayList<Ladron> ladrones = expediente.verCoincidencias(caracteristicas);
		assertTrue(ladrones.isEmpty());
	}
	
	
	@Test
	public void testPruebaQueAlPasarleUnListaDeCaracteristicasQueNoCorrespondenA_LaDeLosLadronesQueContieneElExpedienteDevuelveUnaListaVacia() {
		
		ladron1.agregarCaracteristica(caract1);
		ladron1.agregarCaracteristica(caract2);
		ladron1.agregarCaracteristica(caract3);
		ladron1.agregarCaracteristica(caract4);
		
		ladron2.agregarCaracteristica(caract5);
		ladron2.agregarCaracteristica(caract6);
		ladron2.agregarCaracteristica(caract7);
		
		expediente.agregarLadron(ladron1);
		expediente.agregarLadron(ladron2);
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		caracteristicas.add(caract8);
		
		ArrayList<Ladron> ladrones = expediente.verCoincidencias(caracteristicas);
		assertTrue(ladrones.isEmpty());
	}

	@Test
	public void testPruebaQueAlPasarleUnaListaDeCaracteristicasQueCorrespondenALosLadronesQueContieneElExpeidenteDevuelveUnaListaConLadronesQueCoincidenLasCaracteristicas() {
		
		ladron1.agregarCaracteristica(caract1);
		ladron1.agregarCaracteristica(caract2);
		ladron1.agregarCaracteristica(caract3);
		ladron1.agregarCaracteristica(caract4);
		
		ladron2.agregarCaracteristica(caract5);
		ladron2.agregarCaracteristica(caract6);
		ladron2.agregarCaracteristica(caract7);
		
		expediente.agregarLadron(ladron1);
		expediente.agregarLadron(ladron2);
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		caracteristicas.add(caract5);
		
		ArrayList<Ladron> ladrones = expediente.verCoincidencias(caracteristicas);
		assertTrue(ladrones.contains(ladron1) && ladrones.contains(ladron2));
	}
	
	@Test
	public void testPruebaQueAlPasarleUnListaDeCaracteristicasQueCorrespondenALosLadronesDevuelveUnaListaConUnLadron() {
		
		ladron1.agregarCaracteristica(caract1);
		ladron1.agregarCaracteristica(caract2);
		ladron1.agregarCaracteristica(caract3);
		ladron1.agregarCaracteristica(caract4);
		
		ladron2.agregarCaracteristica(caract5);
		ladron2.agregarCaracteristica(caract6);
		ladron2.agregarCaracteristica(caract7);
		
		expediente.agregarLadron(ladron1);
		expediente.agregarLadron(ladron2);
		
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		caracteristicas.add(caract2);
		
		ArrayList<Ladron> ladrones = expediente.verCoincidencias(caracteristicas);
		assertTrue(ladrones.contains(ladron1));
	}
	
	@Test(expected = ContenedorVacioException.class)
	public void testUnExpedienteVacioDeberiaLanzarExcepcionAlPedirleUnLadronRandom(){
		Expediente expedienteVacio = new Expediente();
		expedienteVacio.darLadronRandom();
	}
}
