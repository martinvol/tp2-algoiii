package unitarias;

import static org.junit.Assert.*;


import model.Pais;
import model.RecorridoLadron;

import org.junit.Before;
import org.junit.Test;

public class RecorridoLadronTests {

	RecorridoLadron recorrido;
	Pais Pais1, Pais2;
	
	@Before
	public void setUp() {
	
		Pais1 = new Pais();
		Pais2 = new Pais();
		recorrido = new RecorridoLadron();
	}
	
	@Test
	public void testPruebaQueLaCantidadDeViejasDaCeroAlCrearlo() {
		assertTrue(recorrido.obtenerCantidadDePaises()==0);
	}
	
	@Test
	public void testNegativoPruebaQueNoPuedeTenerSiguientePaisSiNoSeLeAgregoNingunPaisAlRecorrido() {
		assertFalse(recorrido.siguientePais());
	}

	@Test
	public void testPruebaQueRecorridoPuedeAgregarUnPais() {
		assertTrue(recorrido.agregarPais(Pais1));
	}
	
	@Test
	public void testPruebaQueElRecorridoDevuelvElPaisEnElQueEstaElLadronUnaVezAgregadoUnPais() {
		recorrido.agregarPais(Pais1);
		assertTrue(recorrido.obtenerPaisActualDelLadron()==Pais1);
	}
	
	@Test
	public void testPruebaQueAlAgregarUnPaisLaCantidadDePaisesDelRecorridoEsUno() {
		recorrido.agregarPais(Pais1);
		assertTrue(recorrido.obtenerCantidadDePaises()==1);
	}
	
	@Test
	public void testPruebaQueAlAgregarDosPaisesLaCantidadDePaisesDelRecorridoEsDos() {
		recorrido.agregarPais(Pais1);
		recorrido.agregarPais(Pais2);
		assertTrue(recorrido.obtenerCantidadDePaises()==2);
	}
	
	public void testPruebaQueAlPedirElSiguientePaisEsteDevuelveTrueSiTienePaisesAgregados() {
		recorrido.agregarPais(Pais1);
		recorrido.agregarPais(Pais2);
		assertTrue(recorrido.siguientePais());
	}
	
	public void testPruebaQueDespuesDePedirElSiguientePaisElPaisActualDeberiaSerElSegundoPaisAgregado() {
		recorrido.agregarPais(Pais1);
		recorrido.agregarPais(Pais2);
		recorrido.siguientePais();
		
		assertTrue(recorrido.obtenerPaisActualDelLadron()==Pais2);
		
		assertFalse(recorrido.siguientePais());
		assertFalse(recorrido.siguientePais());
		assertFalse(recorrido.siguientePais());
		
		assertTrue(recorrido.obtenerPaisActualDelLadron()==Pais2);
	}
}
