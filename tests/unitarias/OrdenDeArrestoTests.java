package unitarias;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Caracteristica;
import model.Ladron;
import model.OrdenDeArresto;

public class OrdenDeArrestoTests {

	@Test
	public void testUnaOrdenDeArrestoSinSospechosoDevuelveFalseAlCompararseConOtrosLadrones() {
		OrdenDeArresto orden = new OrdenDeArresto(null);
		Ladron unLadron = new Ladron();
		
		assertFalse(orden.correspondeA(unLadron));
	}
	
	@Test
	public void testUnaOrdenDeArrestoParaUnSospechosoDeberiaCorresponderseConElMismoLadron() {
		
		Ladron sospechoso = new Ladron();
		Ladron otroLadron = new Ladron();
		otroLadron.agregarCaracteristica(new Caracteristica(null, null, null));
		OrdenDeArresto orden = new OrdenDeArresto(sospechoso);
		
		assertTrue(orden.correspondeA(sospechoso));
		assertFalse(orden.correspondeA(otroLadron));
	}

}
