package unitarias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model.Caracteristica;
import model.GradoDificultad.DificultadGrado;
import model.Lugares.*;
import model.Tipo.*;


public class LugaresTests {
	
	private static final String DIFICULTAD_UNO = "1";
	private Tipo tipoMoneda,tipoBandera,tipoAnimales;
	private Lugar lugar1,lugar2,lugar3;
	private Caracteristica caract1,caract2,caract3;
	
	
	@Before
	public void setUp(){
		
		lugar1 = new LugarBanco();
		lugar2 = new LugarAeropuerto();
		lugar3 = new LugarBiblioteca();
		tipoMoneda= new TipoMoneda();
		tipoBandera= new TipoBandera();
		tipoAnimales= new TipoAnimales();
		
		caract1=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract2=new Caracteristica(tipoBandera,"Azul,Blanca, Azul",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract3=new Caracteristica(tipoAnimales,"Zorro",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		
	}
	
	@Test
	public void testPruebaQueUnLugarDevuelvaQueNoTienePistaSiNoSeLeAgregoNingunaCaracteristica() {


		assertTrue(lugar1.sinPistas());
		assertTrue(lugar2.sinPistas());
		assertTrue(lugar3.sinPistas());	
	}
	
	@Test
	public void testPruebaQueUnLugarQueNoSeLeAgregaronPistasDevuelvaUnaPistaVacia() {

		ArrayList<Caracteristica> pistas = lugar1.darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(), "Nunca vi a nadie con esa descripcion");
		pistas = lugar2.darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(), "Nunca vi a nadie con esa descripcion");
		pistas = lugar3.darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(), "Nunca vi a nadie con esa descripcion");
		
		
	}
	
	@Test
	public void testPruebaQueUnLugarPuedeGuardarCaracteristicasCorrectamenteSoloDelTipoQueLeCorresponde() {

		assertTrue(lugar1.agregarCaracteristica(caract1));
		assertTrue(lugar2.agregarCaracteristica(caract2));
		assertTrue(lugar3.agregarCaracteristica(caract3));
		
		assertFalse(lugar3.agregarCaracteristica(caract1));
		assertFalse(lugar3.agregarCaracteristica(caract2));
		assertFalse(lugar2.agregarCaracteristica(caract1));
		assertFalse(lugar2.agregarCaracteristica(caract3));
		assertFalse(lugar1.agregarCaracteristica(caract3));
		assertFalse(lugar1.agregarCaracteristica(caract2));
	}
	
	@Test
	public void testPruebaQueUnLugarDevuelveLasPistasAsignadasCorrectamente() {
		
		
		lugar1.agregarCaracteristica(caract2);
		lugar1.agregarCaracteristica(caract1);
		lugar1.agregarCaracteristica(caract3);
		
		ArrayList<Caracteristica> pistas = lugar1.darPistas();
		assertTrue(pistas.contains(caract1));
		assertFalse(pistas.contains(caract2));
		assertFalse(pistas.contains(caract3));
	}
	

	@Test
	public void testPruebaQueUnLugarSiNoSeLeAsignaUnaPistaEnviaUnaPistaVacia() {

		ArrayList<Caracteristica> pistas = lugar1.darPistas();
		
		assertEquals(pistas.get(0).mostrarComoPista(), "Nunca vi a nadie con esa descripcion");
		
		lugar1.agregarCaracteristica(caract1);
		pistas = lugar1.darPistas();
		
		assertTrue(pistas.contains(caract1));	
	}
	
	@Test
	public void testPruebaQueUnLugarBorraSusPistasPeroSiempreExisteLaPistaVacia() {
		
		lugar1.agregarCaracteristica(caract2);
		lugar1.agregarCaracteristica(caract1);
		
		assertTrue(lugar1.limpiarPistas());
		
		ArrayList<Caracteristica> pistas = lugar1.darPistas();
		
		assertEquals(pistas.get(0).mostrarComoPista(), "Nunca vi a nadie con esa descripcion");
		assertFalse(pistas.contains(caract2));
		assertFalse(pistas.contains(caract1));
	}

	@Test
	public void testPruebaQueUnLugarGuardaCaracteristicasDeLosTiposDeCaracteristicasQueSeLeAsignaron() {

		assertFalse(lugar1.agregarCaracteristica(caract2));
		assertTrue(lugar1.agregarCaracteristica(caract1));
		
		ArrayList<Caracteristica> pistas = lugar1.darPistas();

		assertFalse(pistas.contains(caract2));
		assertTrue(pistas.contains(caract1));
	}
	
}
