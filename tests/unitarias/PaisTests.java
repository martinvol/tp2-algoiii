package unitarias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Caracteristica;
import model.Limitrofe;
import model.Pais;
import model.Excepciones.CaracteristicaNoExistenteException;
import model.GradoDificultad.*;
import model.Lugares.Lugar;
import model.Tipo.*;

import org.junit.Before;
import org.junit.Test;


public class PaisTests {
	
	private static final String DIFICULTAD_UNO = "1";
	private static final String DIFICULTAD_DOS = "2";
	private Pais pais,pais2,pais3;
	private Tipo tipoCiudad, tipoBandera, tipoMoneda, tipoAnimales;
	private Caracteristica caract1,caract2,caract3,pista1,pista2,pista3;
	private Limitrofe limitrofe1,limitrofe2;
	
	@Before
	public void setUp() {
		
		pais = new Pais();
		pais2 = new Pais();
		pais3 = new Pais();
		
		limitrofe1= new Limitrofe(pais,pais2,20);
		limitrofe2= new Limitrofe(pais,pais3,20);

		tipoAnimales = new TipoAnimales();
		tipoCiudad = new TipoCiudad();
		tipoMoneda = new TipoMoneda();
		tipoBandera = new TipoBandera();
		
		caract1=new Caracteristica(tipoCiudad,"Buenos Aires",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract2=new Caracteristica(tipoBandera,"Azul, Blanca y Azul",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		caract3=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_DOS));
		
		pista1=new Caracteristica(tipoAnimales,"Zorro",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		pista2=new Caracteristica(tipoBandera,"Azul, Blanca y Azul",DificultadGrado.darDificultad(DIFICULTAD_UNO));
		pista3=new Caracteristica(tipoMoneda,"Pesos",DificultadGrado.darDificultad(DIFICULTAD_DOS));		
	}

	@Test
	public void testPruebaQuePaisPuedeAgregarCaracteristicasCorrectamente(){
		assertTrue(pais.agregarCaracteristica(caract1));
		assertTrue(pais.agregarCaracteristica(caract2));
		assertTrue(pais.agregarCaracteristica(caract3));
	}
	
	@Test
	public void testPruebaQuePaisDevuelveSusCaracteristicasAsignadasEnUnaLista(){
		pais.agregarCaracteristica(caract1);
		pais.agregarCaracteristica(caract2);
		pais.agregarCaracteristica(caract3);
		ArrayList<Caracteristica> caracteristicas=pais.obtenerCaracteristicas();
		
		assertTrue(caracteristicas.contains(caract1));
		assertTrue(caracteristicas.contains(caract2));
		assertTrue(caracteristicas.contains(caract3));	
	}
	
	@Test
	public void testPruebaQuePaisDevuelveCaracteristicasDeDiferentesDificultadesSegunSePida(){
		assertTrue(pais.agregarCaracteristica(caract1));
		assertTrue(pais.agregarCaracteristica(caract2));
		assertTrue(pais.agregarCaracteristica(caract3));
		
		ArrayList<Caracteristica> caracteristicas = pais.obtenerCaracteristicasDificultad(DificultadGrado.darDificultad(DIFICULTAD_UNO));
		assertTrue(caracteristicas.contains(caract1));
		assertTrue(caracteristicas.contains(caract2));
		assertFalse(caracteristicas.contains(caract3));
		ArrayList<Caracteristica> caracteristicas2 = pais.obtenerCaracteristicasDificultad(DificultadGrado.darDificultad(DIFICULTAD_DOS));
		assertTrue(caracteristicas2.contains(caract3));
		assertFalse(caracteristicas2.contains(caract1));
		assertFalse(caracteristicas2.contains(caract2));
	}
	
	@Test
	public void testPruebaQuePaisDevuelveVerdaderoOFalsoSiContieneLaCaracteristicaPasada(){
		pais.agregarCaracteristica(caract1);
		pais.agregarCaracteristica(caract2);

		
		assertTrue(pais.contieneCaracteristica(caract1));
		assertTrue(pais.contieneCaracteristica(caract2));
		assertFalse(pais.contieneCaracteristica(caract3));
		
	}
	
	@Test
	public void testPruebaQuePaisDevuelveSusLugaresYEstosNoTienenPistas(){
		
		ArrayList<Lugar> lugares= pais.obtenerLugares();
		
		ArrayList<Caracteristica> pistas1 =lugares.get(0).darPistas();
		ArrayList<Caracteristica> pistas2 =lugares.get(1).darPistas();
		ArrayList<Caracteristica> pistas3 =lugares.get(2).darPistas();
		
		assertEquals(pistas1.get(0).mostrarComoPista(),"Nunca vi a nadie con esa descripcion");
		assertEquals(pistas2.get(0).mostrarComoPista(),"Nunca vi a nadie con esa descripcion");
		assertEquals(pistas3.get(0).mostrarComoPista(),"Nunca vi a nadie con esa descripcion");
		
	}
	
	
	@Test
	public void testPruebaQuePaisBorraLasPistasDeLosLugares(){
		
		ArrayList<Lugar> lugares= pais.obtenerLugares();
		
		lugares.get(0).agregarCaracteristica(pista3);
		lugares.get(1).agregarCaracteristica(pista1);
		lugares.get(2).agregarCaracteristica(pista2);
		
		ArrayList<Caracteristica> pistas= lugares.get(0).darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(),"Cambio todo su dinero a Pesos");
		
		assertTrue(pais.limpiarPistas());
		
		pistas= lugares.get(0).darPistas();
		assertEquals(pistas.get(0).mostrarComoPista(),"Nunca vi a nadie con esa descripcion");						
	}
	
	@Test
	public void testPruebaQuePaisPuedeAgregarLimitrofes(){
		
		assertTrue(pais.setLimitrofe(limitrofe1));
		assertFalse(pais.setLimitrofe(limitrofe1));
		assertTrue(pais.setLimitrofe(limitrofe2));	
	}
	
	@Test
	public void testPruebaQuePaisAsignaLimitrofes(){
		
		pais.setLimitrofe(limitrofe1);
		pais.setLimitrofe(limitrofe2);
		ArrayList<Limitrofe> limitrofes= pais.getLimitrofes();
		
		assertEquals(limitrofes.get(0),limitrofe1);
		assertEquals(limitrofes.get(1),limitrofe2);	
	}
	
	@Test
	public void testPruebaQuePaisDevuelveUnaListaVaciaSiNoSeLeAsignaronLimitrofes(){
		
		ArrayList<Limitrofe> limitrofes= pais.getLimitrofes();
		
		assertTrue(limitrofes.isEmpty());
	}
	
}
