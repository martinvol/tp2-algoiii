package unitarias;
import static org.junit.Assert.*;
import model.Tiempo;

import org.junit.Before;
import org.junit.Test;


public class TiempoTests {

	private Tiempo tiempo;
	private int CANTIDAD_DE_TIEMPO = 154; 
	
	@Before
	public void setUp() throws Exception {
		tiempo = new Tiempo(CANTIDAD_DE_TIEMPO);
	}

	@Test
	public void testTiempoRestante() {
		assertEquals("El tiempo inicial es el correcto", tiempo.tiempoRestante(), CANTIDAD_DE_TIEMPO, .1);
		tiempo.restarHoras(10);
		assertEquals("El tiempo inicial es el correcto", tiempo.tiempoRestante(), CANTIDAD_DE_TIEMPO - 10, .1);
	}
	
	@Test
	public void testIndicarQueSeTerminaElTiempoEsCorrecto() {
		
		assertFalse(tiempo.terminoElTiempo());
		tiempo.restarHoras(CANTIDAD_DE_TIEMPO);
		assertTrue(tiempo.terminoElTiempo());
		
	}
	
	@Test
	public void testNoDeberiaPoderRestarseMasTiempoDelInicial() {
		
		assertFalse(tiempo.terminoElTiempo());
		tiempo.restarHoras(CANTIDAD_DE_TIEMPO);
		assertEquals(tiempo.tiempoRestante(), 0, .1);
		
		tiempo.restarHoras(1);
		assertEquals(tiempo.tiempoRestante(), 0, .1);
	}
	
	@Test
	public void dormir(){
		// se descuentan 8 horas todos los dias a las 0 hs
		tiempo.restarHoras(17);
		assertEquals(tiempo.tiempoRestante(), CANTIDAD_DE_TIEMPO-(17 + 8), .1);
	}
	
	@Test
	public void testElTiempoSeDeberiaMostrarCorrectamente() {
		Tiempo tiempo = new Tiempo(CANTIDAD_DE_TIEMPO);
		assertEquals(tiempo.toString(), "Lunes, 7 hs");
		
		tiempo.restarHoras(1);
		assertEquals(tiempo.toString(), "Lunes, 8 hs");
		
		tiempo.restarHoras(10);
		assertEquals(tiempo.toString(), "Lunes, 18 hs");
		
		tiempo.restarHoras(6); // Duerme
		assertEquals(tiempo.toString(), "Martes, 8 hs");
		
		tiempo.restarHoras(15);
		assertEquals(tiempo.toString(), "Martes, 23 hs");
		
		tiempo.restarHoras(3); // Vuelve a dormir
		assertEquals(tiempo.toString(), "Miercoles, 10 hs");
	}
	
	@Test
	public void testElTiempoDebePoderResetearseASuValorInicial() {
		
		Tiempo tiempo = new Tiempo(CANTIDAD_DE_TIEMPO);
		tiempo.restarHoras(CANTIDAD_DE_TIEMPO/2);
		tiempo.resetHoras();
		assertEquals(tiempo.tiempoRestante(), CANTIDAD_DE_TIEMPO, 0);
	}
}
