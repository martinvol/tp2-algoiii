package unitarias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Caracteristica;
import model.Limitrofe;
import model.Mapa;
import model.Pais;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.TipoCiudad;

import org.junit.Before;
import org.junit.Test;

import parser.Parser;

public class CargaDeLimitrofesTest {
	
	Parser parser;
	Mapa mapa;
	
	@Before
	public void setUp() throws Exception {
		parser = new Parser();
		mapa = parser.crearMapa("resources/datos/paises.xml");
		parser.agregarLimitrofes(mapa, "resources/datos/limitrofes.xml");
	}

	@Test
	public void testPruebaQueUnPaisDevuelveTresOCuatroLimitrofes() {
		Caracteristica caracteristica = new Caracteristica(new TipoCiudad(),"Buenos Aires",DificultadGrado.darDificultad("1"));
		Pais pais = mapa.obtenerPaisPorCiudad(caracteristica);
		assertTrue(pais.getLimitrofes().size() == 3);
	}
	
	@Test
	public void testPruebaQueUnPaisCoincideEnDistancia(){
		Caracteristica caracteristica = new Caracteristica(new TipoCiudad(),"Tokio",DificultadGrado.darDificultad("1"));
		Pais paisPartida = mapa.obtenerPaisPorCiudad(caracteristica);
		ArrayList<Limitrofe> limitrofes1 = paisPartida.getLimitrofes();
		Limitrofe limitrofe1 =limitrofes1.get(1);
		Double paisLLegada = limitrofe1.obtenerDistancia();
		assertTrue(paisLLegada == 9854);
	}

}
