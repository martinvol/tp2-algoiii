package unitarias;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import model.*;
import model.GradoDificultad.*;
import model.Tipo.TipoCiudad;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import parser.Parser;

import java.util.*;

public class CargaPaisesTests {
	
	Parser parser;
	Mapa mapa;
	ArrayList<Pais> paises;
	
	@Before
	public void setUp() throws ParserConfigurationException, TransformerException, SAXException, IOException {
		parser = new Parser();
		mapa = parser.crearMapa("resources/datos/paises.xml");
		paises = mapa.getMapa();	

	}	

	@Test
	public void testPruebaQueMapaTieneListaDePaises() {
		
		assertNotNull(paises);
	}
	
	
	@Test
	public void testPruebaQuePaisTieneCaracteristicas() {
		
		TipoCiudad tipociudad = new TipoCiudad();
		String descripcion = "Atenas";
		DificultadGrado dificultad = DificultadGrado.darDificultad("1");
		Caracteristica caracteristica = new Caracteristica(tipociudad,descripcion,dificultad);
		Pais pais = mapa.obtenerPaisPorCiudad(caracteristica);
		assertTrue(pais.contieneCaracteristica(caracteristica));
	}

	@Test
	public void testPruebaQueSeCargaronLos30PaisesDelMapa() {
		
		int cantidadPaises = paises.size();
		assertTrue(cantidadPaises == 30);
	}

	
	@Test
	public void testPruebaQueSeDevuelvaListaDeCaracteristicasConGradoDeDificultad() {
		
		TipoCiudad tipociudad = new TipoCiudad();
		String descripcion = "Bamako";
		DificultadGrado dificultad = DificultadGrado.darDificultad("1");
		Caracteristica caracteristica = new Caracteristica(tipociudad,descripcion,dificultad);
		Pais pais = mapa.obtenerPaisPorCiudad(caracteristica);
		ArrayList<Caracteristica> caracteristicas = pais.obtenerCaracteristicasDificultad(DificultadGrado.darDificultad("4"));
		assertTrue(caracteristicas.size() == 2);
	}
}
