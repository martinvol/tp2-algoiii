package unitarias;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Caracteristica;
import model.Expediente;
import model.Ladron;
import model.GradoDificultad.DificultadGrado;
import model.Tipo.TipoPelo;
import model.Tipo.TipoVehiculo;

import org.junit.Before;
import org.junit.Test;

import parser.Parser;

public class CargaDeLadronesTest {
	
	Parser parser;
	Expediente expediente;

	@Before
	public void setUp() throws Exception {
		Parser parser = new Parser();
		this.expediente = parser.crearExpediente("resources/datos/ladrones.xml");
	}

	@Test
	public void testPruebaQueExpedienteNoEsNull() {
		assertNotNull(this.expediente);
	}

	@Test
	public void testPruebaQueMeDevuelveLadronesConCoincidenciaDeCaracteristicas(){
		Caracteristica caracteristica = new Caracteristica(new TipoPelo(),"Rubio",DificultadGrado.darDificultad("0"));
		Caracteristica caracteristica2 = new Caracteristica(new TipoVehiculo(),"Convertible",DificultadGrado.darDificultad("0"));
		ArrayList<Caracteristica> caracteristicas = new ArrayList<Caracteristica>();
		ArrayList<Caracteristica> caracteristicas2 = new ArrayList<Caracteristica>();
		caracteristicas.add(caracteristica);
		caracteristicas2.add(caracteristica);
		caracteristicas2.add(caracteristica2);
		ArrayList <Ladron> ladronesConCaracteristicaRubio = expediente.verCoincidencias(caracteristicas);
		ArrayList <Ladron> ladronesConCaracteristicaRubioYConvertible = expediente.verCoincidencias(caracteristicas2);
		assertTrue(ladronesConCaracteristicaRubio.size() == 2); //en el expediente hay dos ladrones con pelo Rubio
		assertTrue(ladronesConCaracteristicaRubioYConvertible.size() == 0); //en el expediente no hay ningun ladron con pelo Rubio y Convertible
		
	}
}
